/*
 * ipc.c
 *
 *  Created on: Oct 29, 2017
 *      Author: netcat
 */

#include "sys.h"
#include "ipc.h"
#include <stdlib.h>

xQueueHandle ipc_q[IPC_CLIENT_MAX] = {};

void ipc_init(ipc_client_t client)
{
  ipc_q[client] = xQueueCreate(IPC_QUEUE_SIZE, sizeof(ipc_msg_t));
}

int ipc_poll(ipc_client_t client, ipc_msg_t *msg)
{
  return xQueueReceive(ipc_q[client], msg, IPC_POLL_TIMEOUT) != errQUEUE_EMPTY;
}

void ipc_send(ipc_client_t from, ipc_client_t to, uint32_t type, uint32_t arg, const void *data, uint32_t size)
{
  ipc_msg_t msg = { .from = from, .to = to, .type = type, .arg = arg, .size = size, .data = 0 };
  if(size)
  {
    msg.data = pvPortMalloc(size);
    memcpy(msg.data, data, size);
  }
  xQueueSendToBack(ipc_q[to], &msg, IPC_POLL_TIMEOUT);
}
