/*
 * syslog.c
 *
 *  Created on: Nov 2, 2018
 *      Author: netcat
 */

#include "sys.h"
#include "syslog.h"
#include <stdarg.h>

static char last_syslog_msg[MAX_SYSLOG_MESSAGE];

const char *color_str[] =
{
  "\x1B[0;31m", // red
  "\x1B[0;33m", // yellow
  "",
  "\x1B[1;30m", // gray
};

static void vsyslog(uint32_t pri, const char *fmt, va_list args)
{
  vsnprintf(last_syslog_msg, MAX_SYSLOG_MESSAGE - 1, fmt, args);
  last_syslog_msg[MAX_SYSLOG_MESSAGE - 1] = '\0';

  if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED)
  {
    printf("%s\n", last_syslog_msg);
  }
  else
  {
    int t = xTaskGetTickCount();
    const char *name = pcTaskGetName(xTaskGetCurrentTaskHandle());

    // remote \n
    int i;
    for(i = 0; i < MAX_SYSLOG_MESSAGE; i++) if(last_syslog_msg[i] == '\n') last_syslog_msg[i] = ' ';

    printf("\x1B[0;32m[ % 5d ]\x1B[0m \x1B[0;33m%s:\x1B[0m %s%s \x1B[0m\n", t, name, color_str[pri], last_syslog_msg);
  }
}

void syslog(uint32_t pri, const char *fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  vsyslog(pri, fmt, args);
  va_end(args);
}

void slog(const char * fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  vsyslog(SYSLOG_INFO, fmt, args);
  va_end(args);
}

