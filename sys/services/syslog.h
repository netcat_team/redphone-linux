/*
 * syslog.h
 *
 *  Created on: Nov 2, 2018
 *      Author: netcat
 */

#ifndef SYSLOG_H_
#define SYSLOG_H_

#include <stdint.h>

#define MAX_SYSLOG_MESSAGE 128

typedef struct
{
  uint32_t pid;
  uint32_t tick_count;
  uint8_t pri;
  uint8_t res[2];
  char *msg[MAX_SYSLOG_MESSAGE];
} syslog_record_t;

enum
{
  SYSLOG_ERROR = 0,
  SYSLOG_WARN,
  SYSLOG_INFO,
  SYSLOG_DEBUG,
};

void syslog(uint32_t pri, const char *fmt, ...);
void slog(const char * fmt, ...);

#endif /* SYSLOG_H_ */
