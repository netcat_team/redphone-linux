/*
 * ipc.h
 *
 *  Created on: Oct 29, 2017
 *      Author: netcat
 */

#ifndef SYS_IPC_H_
#define SYS_IPC_H_

#define IPC_QUEUE_SIZE 32
#define IPC_POLL_TIMEOUT 1

typedef enum 
{
  IPC_GSM_SRV,
  IPC_GSM_DRIVER,

  IPC_CLIENT_MAX
} ipc_client_t;

typedef struct
{
  ipc_client_t from;
  ipc_client_t to;
  uint32_t type;
  uint32_t arg;
  uint32_t size;
  uint8_t *data;
} ipc_msg_t;

void ipc_init(ipc_client_t client);
int ipc_poll(ipc_client_t client, ipc_msg_t *msg);
void ipc_send(ipc_client_t from, ipc_client_t to, uint32_t type, uint32_t arg, const void *data, uint32_t size);

#endif /* SYS_IPC_H_ */
