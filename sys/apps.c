#include "apps.h"
#include <string.h>
#include "sys/services/syslog.h"

// ld
extern int __apps_db_start;
extern int __apps_db_end;

apps_table_st *apps_db = (apps_table_st*)&__apps_db_start;
int apps_db_size = 0;

apps_table_st *apps_find(char *str)
{
  int cnt = apps_db_size;
  apps_table_st *app = apps_db;
  while(cnt--)
  {
    if(!strcmp(app->str, str)) return app;
    app++;
  }
  return 0;
}

int apps_run(int argc, char *argv[])
{
  for(int i = 0; i < argc; i++)
  {
    printf(" |%s|", argv[i]);
    printf("\n");
  }

  if(!strcmp(argv[0], "?"))
  {
    apps_list();
  }
  else
  {
    apps_table_st *app = apps_find(argv[0]);
    if(app)
    {
      app->cli(argc, argv);
      return 1;
    }
    else
    {
      syslog(SYSLOG_DEBUG, "command not found");
    }
  }

  return 0;
}

void apps_list()
{
  int cnt = apps_db_size;
  apps_table_st *app = apps_db;
  while(cnt--)
  {
    syslog(SYSLOG_DEBUG, "%s", app->str);
    app++;
  }
}

void apps_init()
{
  apps_db_size = (int)((&__apps_db_end - &__apps_db_start)) / (sizeof(apps_table_st) >> 2);
  syslog(SYSLOG_DEBUG, "load apps: %d", apps_db_size);
  // int cnt = apps_db_size;
  // apps_table_st *app = apps_db;
  // while(cnt--)
  // {
  //   if(app->init) app->init();
  //   app++;
  // }
}


