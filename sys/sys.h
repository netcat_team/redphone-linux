/*
 * sys.h
 *
 *  Created on: Sep 15, 2017
 *      Author: netcat
 */

#ifndef SYS_H_
#define SYS_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

// libs
#include "freertos/include/FreeRTOS.h"
#include "freertos/include/task.h"
#include "freertos/include/queue.h"
#include "freertos/include/timers.h"
#include "freertos/include/semphr.h"
#include "fatfs/mbr.h"
#include "fatfs/fatfs.h"

// servises
#include "services/ipc.h"
#include "sys/services/syslog.h"
#include "sys/apps.h"
#include "sys/cli.h"
 
// drivers
#include "drivers/gsm.h"

// platform bsp
#include "bsp.h"

#endif /* SYS_H_ */
