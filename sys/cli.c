/*
 * cli.c
 *
 *  Created on: Nov 4, 2018
 *      Author: netcat
 */

#include "cli.h"
#include "sys.h"

static microrl_t rl;
static xQueueHandle cli_rx_queue;

void print(const char * str)
{
  printf("%s", str);
}

void sio_tack(void *pvParameters)
{
  while(1)
  {
    char ch;
    while(xQueueReceive(cli_rx_queue, &ch, 1000) != errQUEUE_EMPTY)
    {
      microrl_insert_char(&rl, ch);
    }
    vTaskDelay(1);
  }
}

void cli_init()
{
  cli_rx_queue = xQueueCreate(128, 1);
  xTaskCreate(sio_tack, "SIO", 2048, 0, 2, 0);

  microrl_init(&rl, print);
  microrl_set_execute_callback(&rl, apps_run);
  microrl_set_complete_callback(&rl, 0);
}

void USART1_IRQHandler()
{
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  uint8_t c;
  if(serial_get(&c))
  {
    xQueueSendToBackFromISR(cli_rx_queue, &c, &xHigherPriorityTaskWoken);
  }
  portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}

