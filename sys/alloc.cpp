
extern "C" {
#include "sys/sys.h"
}
// #include <stdlib.h>

//int vPortSize( void *pv );
//#define malloc_size(t)
//
//void *malloc(size_t size)
//{
//  return pvPortMalloc(size);
//}
//
//void *realloc(void *ptr, size_t size)
//{
//    void *_new;
//
//    if(!ptr)
//    {
//      _new = malloc(size);
//      if(!_new) goto error;
//    }
//    else
//    {
//      size_t t = vPortSize(ptr);
//        if(t < size)
//        {
//          _new = malloc(size);
//          if(!_new) goto error;
//          memcpy(_new, ptr, t);
//          free(ptr);
//        }
//        else
//        {
//          _new = ptr;
//        }
//    }
//
//    return _new;
//error:
//    return NULL;
//}
//
//void free(void *ptr)
//{
//  vPortFree(ptr);
//}

void *operator new(size_t size)
{
//  syslog(SYSLOG_ERROR, "+: %d", size);
  return pvPortMalloc(size);
}

void *operator new[](size_t size)
{
//  syslog(SYSLOG_ERROR, "++: %d", size)
  return pvPortMalloc(size);
}

void operator delete(void *ptr)
{
//  syslog(SYSLOG_ERROR, "-");
  vPortFree(ptr);
}

void operator delete[](void *ptr)
{
//  syslog(SYSLOG_ERROR, "--");
  vPortFree(ptr);
}
