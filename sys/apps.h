/*
 * apps.h
 *
 *  Created on: Sep 14, 2017
 *      Author: netcat
 */

#ifndef SYS_APPS_H_
#define SYS_APPS_H_

// APP macro
// or use:
// apps_table_st test_app  __attribute__((section(".apps_table"))) = { "test", 0, test_main };
#define SYS_APP(name) \
        int name##_main(int argc, char **argv); \
        apps_table_st name##_app  __attribute__((section(".apps_table"))) = { #name, 0, name##_main};

typedef struct
{
  const char *str;
  int (*init)(void);
  int (*cli)(int argc, char **argv);
  int res;
} apps_table_st;

void apps_list(void);
void apps_init(void);
int apps_run(int argc, char *argv[]);

#endif /* APPS_APPS_H_ */
