/*
 * cli.h
 *
 *  Created on: Nov 4, 2018
 *      Author: netcat
 */

#ifndef CLI_H_
#define CLI_H_

#include "libs/microrl/microrl.h"

void cli_init();
void cli_poll();

#endif /* SYS_H_ */
