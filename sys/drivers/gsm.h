/*
 * gsm.h
 *
 *  Created on: Mar 28, 2018
 *      Author: netcat
 */

#ifndef SYS_MODEM_GSM_H_
#define SYS_MODEM_GSM_H_

typedef enum
{
  GSM_DRIVER_MSG_POWER_DOWN,
  GSM_DRIVER_MSG_POWER_UP, // todo
  GSM_DRIVER_MSG_RING,
  GSM_DRIVER_MSG_AOH,
  GSM_DRIVER_MSG_RSSI,
  GSM_DRIVER_MSG_NO_ANSWER,
  GSM_DRIVER_MSG_BUSY,
  GSM_DRIVER_MSG_OUTGOING_CALL,
} gsm_srv_msg_e;

typedef enum
{
  // cmd
  GSM_AT = 0,
  GSM_ATE,  // en/dis echo
  GSM_ATV,  // digital/text mode
  GSM_ATD,  // dial
  GSM_ATH,  // hung up
  GSM_ATA,  // pick up

  // ack
  GSM_OK,       // ok
  GSM_SHORT_OK,
  GSM_ERROR,    // err
  GSM_RING,     // call
  GSM_BUSY,
  GSM_NO_CARRIER,

  // notify
  GSM_RDY,
  GSM_CFUN,
  GSM_CPIN,
  GSM_CMTI_SMS, // sms
  GSM_CLIP,     // caller number
  GSM_CUSD_USSD,        // ussd
  GSM_CMTE_BAD_TEMP,    // bad temperature
  GSM_SIM_STATUS,
  GSM_SIGNAL_STATUS, // rssi
  GSM_CMEE,
  GSM_IMEI,
  GSM_CPAS,
  GSM_CSQ,
  GSM_COPS,
  GSM_CREG,
  GSM_GSMBUSY,
  GSM_CallReady,
  GSM_SmsReady,

  GSM_CMD_MAX
} gsm_opcode_e;

typedef enum
{
  GSM_STATE_NO_INIT,
  GSM_STATE_PWR_DOWN,
  GSM_STATE_PWR_UP,
  GSM_STATE_TEST,
  GSM_STATE_CFG_ECHO,
  GSM_STATE_CFG_BIN_MODE,
  GSM_STATE_CFG_ERR_LVL,
  GSM_STATE_SET_BUSY_ST,
  GSM_STATE_CFG_AOH,
  GSM_STATE_GET_IMEI,
  GSM_STATE_IDLE,
  GSM_STATE_GET_SIM_ST,
  GSM_STATE_GET_REG,
  GSM_STATE_GET_SIGNAL,
  GSM_STATE_GET_OPERATOR,
  GSM_STATE_OUTGOING_CALL,
  GSM_STATE_HANG_UP,
  GSM_STATE_PICK_UP

} gsm_state_e;

typedef enum
{
  GSM_SUBSTATE_PWR_DOWN = 1,
  GSM_SUBSTATE_IDLE = 1,

} gsm_substate_e;

typedef struct
{
  char *buf;
  char *buf_p;
  gsm_state_e state;
  gsm_substate_e substate;
  char num_out[16];
  char num_in[16];
  char text_buf[32];
  int rssi;

  int ret_cnt;
  int ret_flag;
  int upd_flag;
} gsm_db_t;

typedef struct
{
  void (*f)();
  gsm_state_e on_ok;
  gsm_state_e on_err;
  void (*leave)(int error);
} gsm_node_t;

void gsm_service_notify(int opcode);
void gsm_call_cmd();
void gsm_rd_cmd(gsm_opcode_e opcode, int get);
void gsm_wr_cmd(gsm_opcode_e opcode, int set, int val);
int gsm_init();

void gsm_set_state(gsm_state_e state, int is_ok);

#endif /* SYS_MODEM_GSM_H_ */
