/*
 * gsm_fsm.h
 *
 *  Created on: Mar 28, 2018
 *      Author: netcat
 */

#ifndef SYS_MODEM_GSM_FSM_H_
#define SYS_MODEM_GSM_FSM_H_

void gsm_pwr_down();
void gsm_pwr_up();
void gsm_test();
void gsm_config_echo();
void gsm_config_err_lvl();
void gsm_get_sim_st();
void gsm_config_busy_st();
void gsm_get_imei();
void gsm_config_bin();
void gsm_config_aoh();
void gsm_get_signal();
void gsm_get_operator();
void gsm_get_reg();
void gsm_outgoing_call();
void gsm_hang_up();
void gsm_pick_up();

void outgoing_call_complate(int is_ok);

#endif /* SYS_MODEM_GSM_FSM_H_ */
