#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// #include "tty_uart.h"
#include "gsm.h"
#include "gsm_fsm.h"
#include "sys/sys.h"
#include "sys/services/ipc.h"

const char *gsm_opcode_str[] = {
  // cmd
  " ",
  "E",
  "V",
  "D",
  "H",
  "A",

  // ack
  "OK",
  "0",
  "ERROR",
  "RING", // call
  "BUSY", // call
  "NO CARRIER",

  // notify
  "RDY",
  "+CFUN",
  "+CPIN",
  "+CMTI", // sms
  "+CLIP", // caller number
  "+CUSD", // ussd
  "+CMTE", // bad temperature
  "+CSDT?",
  "+CSQ",  // rssi
  "+CMEE",
  "+GSN",
  "+CPAS",
  "+CSQ",
  "+COPS",
  "+CREG",
  "+GSMBUSY",
  "Call Ready",
  "SMS Ready",
};

gsm_db_t db;
xQueueHandle rx_queue = 0;

gsm_node_t fsm[] = {
{ 0,                  0,                      0,                  0,                        }, // GSM_STATE_NO_INIT
{ gsm_pwr_down,       0,                      0,                  0,                        }, // GSM_STATE_PWR_DOWN
{ gsm_pwr_up,         0,                      0,                  0,                        }, // GSM_STATE_PWR_UP
{ gsm_test,           GSM_STATE_CFG_ECHO,     GSM_STATE_PWR_DOWN, 0,                        }, // GSM_STATE_TEST
{ gsm_config_echo,    GSM_STATE_CFG_BIN_MODE, GSM_STATE_PWR_DOWN, 0,                        }, // GSM_STATE_FG_ECHO
{ gsm_config_bin,     GSM_STATE_CFG_ERR_LVL,  GSM_STATE_PWR_DOWN, 0,                        }, // GSM_STATE_CFG_BIN
{ gsm_config_err_lvl, GSM_STATE_SET_BUSY_ST,  GSM_STATE_PWR_DOWN, 0,                        }, // GSM_STATE_CFG_ERR_LVL
{ gsm_config_busy_st, GSM_STATE_CFG_AOH,      GSM_STATE_PWR_DOWN, 0,                        }, // GSM_STATE_SET_BUSY_ST
{ gsm_config_aoh,     GSM_STATE_GET_IMEI,     GSM_STATE_PWR_DOWN, 0,                        }, // GSM_STATE_CFG_AOH
{ gsm_get_imei,       GSM_STATE_IDLE,         GSM_STATE_PWR_DOWN, 0,                        }, // GSM_STATE_GET_IMEI
{ 0,                  0,                      0,                  0,                        }, // GSM_STATE_IDLE
{ gsm_get_sim_st,     GSM_STATE_GET_REG,      GSM_STATE_IDLE,     0,                        }, // GSM_STATE_GET_SIM_ST
{ gsm_get_reg,        GSM_STATE_GET_SIGNAL,   GSM_STATE_IDLE,     0,                        }, // GSM_STATE_GET_REG
{ gsm_get_signal,     GSM_STATE_GET_OPERATOR, GSM_STATE_IDLE,     0,                        }, // GSM_STATE_GET_SIGNAL
{ gsm_get_operator,   GSM_STATE_IDLE,         GSM_STATE_IDLE,     0,                        }, // GSM_STATE_GET_OPERATOR
{ gsm_outgoing_call,  GSM_STATE_IDLE,         GSM_STATE_IDLE,     outgoing_call_complate,   }, // GSM_STATE_OUTGOING_CALL
{ gsm_hang_up,        GSM_STATE_IDLE,         GSM_STATE_IDLE,     0,                        }, // GSM_STATE_HANG_UP
{ gsm_pick_up,        GSM_STATE_IDLE,         GSM_STATE_IDLE,     0,                        }, // GSM_STATE_PICK_UP
};

void ptr(const char *str)
{
  int i, len = strlen(str);
  printf("[%d] ", len);
  for(i = 0; i < len; i++)
  {
    printf("%x ", (unsigned int)str[i]);
  }
  printf("\n");
}

void str_trim(char *str, char symb)
{
  char *t = str;

  // left trim
  while(*t == symb)
  {
    // left shift
    char *c = t;
    while(*c)
    {
      *c = *(c + 1);
      c++;
    }
  }

  // right trim
  t = str + strlen(str) - 1;
  while(*t == symb)
  {
    *t = 0;
    t--;
  }
}

const char *str_cut(const char *str, int cnt, char symb, char *buf)
{
  const char *st = str;
  const char *end = str;

  do
  {
    st = end;
    end = strchr(st, symb);
    if(end)
    {
      int len = end - st;
      memcpy(buf, st, len);
      buf[len] = 0;
      end++;
    }
    else return 0;
  } while(cnt-- && *end);

  str_trim(buf, ' ');
  str_trim(buf, '\"');

  return end;
}

void gsm_service_notify(int opcode)
{
  ipc_send(IPC_GSM_DRIVER, IPC_GSM_SRV, opcode, 0, 0, 0);
}

void gsm_service_notify_arg(int opcode, int arg)
{
  ipc_send(IPC_GSM_DRIVER, IPC_GSM_SRV, opcode, arg, 0, 0);
}

void gsm_call_cmd()
{
  char str[32];
  int len = sprintf(str, "ATD%s;\n", db.num_out);
  syslog(SYSLOG_DEBUG, "\x1B[1;34m> %s\x1B[0m", str);
  modem_serial_out(str, len);
}

void gsm_rd_cmd(gsm_opcode_e opcode, int get)
{
  char str[16];
  int len = sprintf(str, "AT%s%s\n", gsm_opcode_str[opcode], get ? "?" : "");
  syslog(SYSLOG_DEBUG, "\x1B[1;34m> %s\x1B[0m", str);
  modem_serial_out(str, len);
}

void gsm_wr_cmd(gsm_opcode_e opcode, int set, int val)
{
  char str[16];
  int len = sprintf(str, "AT%s%s%d\n", gsm_opcode_str[opcode], set ? "=" : "", val);
  syslog(SYSLOG_DEBUG, "\x1B[1;34m> %s\x1B[0m", str);
  modem_serial_out(str, len);
}

TimerHandle_t tmr_retry, tmr_update;

void gsm_state_emit()
{
//  printf("emit state: %d\n", db.state);

  if(fsm[db.state].f)
  {
    if(fsm[db.state].on_ok && fsm[db.state].on_err) xTimerStart(tmr_retry, 0);
    fsm[db.state].f();
  }
}

void gsm_state_halt()
{
  xTimerStop(tmr_retry, 0);
  db.ret_cnt = 0;
  db.ret_flag = 0;

  gsm_set_state(GSM_STATE_PWR_DOWN, 0);
}

void gsm_state_end(int is_ok)
{
  xTimerStop(tmr_retry, 0);
  db.ret_cnt = 0;
  db.ret_flag = 0;

  if(is_ok) gsm_set_state(fsm[db.state].on_ok, is_ok);
  else gsm_set_state(fsm[db.state].on_err, is_ok);
}

void gsm_set_state(gsm_state_e state, int is_ok)
{
  syslog(SYSLOG_DEBUG, "GSM state: %d\n", state);

  if(!state) return;

  if(fsm[db.state].leave) fsm[db.state].leave(is_ok);
  db.state = state;
  gsm_state_emit();
}

void gsm_set_sub_state(gsm_state_e state)
{
//  printf("upd state: %d\n", state);

  if(!state) return;
  db.state = state;
}

gsm_opcode_e gsm_decode(const char *buf)
{
  uint32_t i;
  for(i = 0; i < GSM_CMD_MAX; i++)
  {
    uint32_t len = strlen(gsm_opcode_str[i]);
    if(!strncmp(gsm_opcode_str[i], (char*)buf, len))
    {
//      printf("accept: %s\n", gsm_opcode_str[i]);
      return i;
    }
    else
    {
//      printf("wrong: %s\n", gsm_opcode_str[i]);
    }
  }
  return i;
}

void gsm_parce_notification(const char *str, char *buf)
{
  const char *t = str_cut(str, 0, ':', buf);

  if(t)
  {
    gsm_opcode_e ans = gsm_decode(buf);
    if(ans == GSM_SIGNAL_STATUS)
    {
      if(str_cut(t, 0, ',', buf))
      {
        int rssi = atoi(buf);

        if(db.rssi != rssi)
        {
          db.rssi = rssi;
          gsm_service_notify_arg(GSM_DRIVER_MSG_RSSI, rssi);
        }
      }
    }
    else if(ans == GSM_CLIP)
    {
      if(str_cut(t, 0, ',', buf))
      {
        int len = strlen(buf);
        ipc_send(IPC_GSM_DRIVER, IPC_GSM_SRV, GSM_DRIVER_MSG_AOH, 0, buf, len);
      }
    }
  }
}

void tmr_retry_handle(TimerHandle_t xTimer)
{
//  printf("retry\n");
  xTimerStop(tmr_retry, 0);
  db.ret_flag = 1;
}

void tmr_update_handle(TimerHandle_t xTimer)
{
//  printf("update\n");
  xTimerStop(tmr_update, 0);
}

void gsm_task(void *pvParameters)
{
  // db init
  db.buf = pvPortMalloc(32);
  db.buf_p = db.buf;
  db.state = GSM_STATE_NO_INIT;
  db.substate = GSM_SUBSTATE_PWR_DOWN;
  rx_queue = xQueueCreate(128, 1);

  // hw init
  ipc_init(IPC_GSM_DRIVER);
  if(!modem_serial_init())
  {
    gsm_set_state(GSM_STATE_PWR_DOWN, 1);
  }
  else
  {
    syslog(SYSLOG_ERROR, "modem driver stoped");
  }

  while(1)
  {
    char data;
    if(xQueueReceive(rx_queue, &data, 100) != errQUEUE_EMPTY)
    {
//      printf("in: 0x%x\n", (unsigned)data);

      if(data == '\r' || (data == '\n' && db.buf_p == db.buf))
      {
        // drop
//          printf("drop empty\n");
      }
      else if(data == '\n')
      {
        *db.buf_p = 0;
        db.buf_p = db.buf;

        if(db.buf[0] != 'A' && // drop echo
           db.buf[1] != 'T')
        {
//          ptr(db.buf);
          syslog(SYSLOG_DEBUG, "   %s\n", db.buf);

          if(db.buf[0] == '+')
          {
//            printf("notify\n");
            gsm_parce_notification(db.buf, db.text_buf);
          }
          else
          {
            gsm_opcode_e ans = gsm_decode(db.buf);
            if(ans != GSM_CMD_MAX)
            {
              if(ans == GSM_OK) gsm_state_end(1);
              else if(ans == GSM_ERROR) gsm_state_end(0);
              else if(ans == GSM_RING) gsm_service_notify(GSM_DRIVER_MSG_RING);
              else if(ans == GSM_NO_CARRIER) gsm_service_notify(GSM_DRIVER_MSG_NO_ANSWER);
              else if(ans == GSM_BUSY) gsm_service_notify(GSM_DRIVER_MSG_BUSY);
              else if(ans > GSM_NO_CARRIER)
              {
//                printf("notify\n");
              }
              else
              {
                syslog(SYSLOG_ERROR, "unsupported answer");
                gsm_state_end(0);
              }
            }
            else
            {
//              printf("unknown!\n");
            }
          }
        }
        else
        {
  //          printf("drop echo\n");
        }
      }
      else
      {
        *db.buf_p++ = data;
  //      printf("+\n");
      }
    }
    if(db.state == GSM_STATE_IDLE ||
       db.state == GSM_STATE_PWR_DOWN)
    {
      ipc_msg_t msg;
      if(ipc_poll(IPC_GSM_DRIVER, &msg))
      {
        if(msg.type == GSM_STATE_PWR_UP) gsm_set_state(GSM_STATE_PWR_UP, 1);
        else if(msg.type == GSM_STATE_GET_SIM_ST) gsm_set_state(GSM_STATE_GET_SIM_ST, 1);
        else if(msg.type == GSM_STATE_HANG_UP) gsm_set_state(GSM_STATE_HANG_UP, 1);
        else if(msg.type == GSM_STATE_PWR_UP) gsm_set_state(GSM_STATE_PWR_UP, 1);
        else if(msg.type == GSM_STATE_PICK_UP) gsm_set_state(GSM_STATE_PICK_UP, 1);
        else if(msg.type == GSM_STATE_OUTGOING_CALL)
        {
          memcpy(db.num_out, msg.data, msg.size);
          gsm_set_state(GSM_STATE_OUTGOING_CALL, 1);
        }
      }
    }
    if(db.ret_flag)
    {
      db.ret_flag = 0;

      db.ret_cnt++;
      if(db.ret_cnt < 3) gsm_state_emit();
      else gsm_state_halt();
    }
    if(db.state == GSM_STATE_IDLE)
    {
      if(!xTimerIsTimerActive(tmr_update))
      {
        xTimerStart(tmr_update, 0);
        gsm_set_state(GSM_STATE_GET_SIM_ST, 1);
      }
    }
    vTaskDelay(1);
  }
}

int gsm_init()
{
  tmr_retry = xTimerCreate("serial retry", 3000, pdFALSE, (void *)0, tmr_retry_handle);
  tmr_update = xTimerCreate("network check", 5000, pdFALSE, (void *)0, tmr_update_handle);
  xTaskCreate(gsm_task, "GSM", 1024, 0, configMAX_PRIORITIES - 5, 0);
  return 0;
}



