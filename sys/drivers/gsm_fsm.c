/*
 * gsm_fsm.c
 *
 *  Created on: Mar 28, 2018
 *      Author: netcat
 */

#include <stdio.h>

#include "gsm.h"
#include "gsm_fsm.h"

void gsm_pwr_down()
{
//  gsm_set_sub_state(GSM_SUBSTATE_PWR_DOWN);
//  printf("\x1B[1;34m > pwr_down\x1B[0m \n");
  gsm_service_notify(GSM_DRIVER_MSG_POWER_DOWN);
}

void gsm_pwr_up()
{
//  printf("\x1B[1;34m > pwr up\x1B[0m \n");
  gsm_set_state(GSM_STATE_TEST, 1);
}

void gsm_test()
{
  gsm_rd_cmd(GSM_AT, 0);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_config_echo()
{
  gsm_wr_cmd(GSM_ATE, 0, 1);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_config_err_lvl()
{
  gsm_wr_cmd(GSM_CMEE, 1, 0);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_get_sim_st()
{
  gsm_rd_cmd(GSM_CPIN, 1);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_config_busy_st()
{
  gsm_wr_cmd(GSM_GSMBUSY, 1, 0);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_get_imei()
{
  gsm_rd_cmd(GSM_IMEI, 0);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_config_bin()
{
  gsm_wr_cmd(GSM_ATV, 0, 1);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_config_aoh()
{
  gsm_wr_cmd(GSM_CLIP, 1, 1);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_get_signal()
{
  gsm_rd_cmd(GSM_CSQ, 0);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_get_operator()
{
  gsm_rd_cmd(GSM_COPS, 1);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_get_reg()
{
  gsm_rd_cmd(GSM_CREG, 1);
//  printf("\x1B[0;35m    %s\x1B[0m \n", __FUNCTION__);
}

void gsm_outgoing_call()
{
  gsm_call_cmd();
}

void gsm_hang_up()
{
  gsm_wr_cmd(GSM_ATH, 0, 0);
}

void gsm_pick_up()
{
  gsm_rd_cmd(GSM_ATA, 0);
}

void outgoing_call_complate(int is_ok)
{
  if(!is_ok) return;
  gsm_service_notify(GSM_DRIVER_MSG_OUTGOING_CALL);
}

