/*
 * ps.cpp
 *
 *  Created on: Oct 10, 2017
 *      Author: netcat
 */

#include <xsystem.h>

SYS_APP(ps);

const char *task_state[] = { "Run", "Rdy", "Blo", "Susp", "Del", "Inv" };
 
int ps_main(int argc, char **argv)
{
  UBaseType_t n = uxTaskGetNumberOfTasks();
  TaskStatus_t *list = (TaskStatus_t *)pvPortMalloc(n * sizeof(TaskStatus_t));

  if(list)
  {
    n = uxTaskGetSystemState(list, n, 0);
    printf("%s\n", "Name\tState\tPri\tStack\tNum\tRT");

    for(uint32_t i = 0; i < n; i++)
    {
      XString str(XString(list[i].pcTaskName) + "\t" +
        XString(task_state[list[i].eCurrentState]) + "\t" +
        XString::number(list[i].uxCurrentPriority) + "\t" +
        XString::number(list[i].usStackHighWaterMark) + "\t" + // uxTaskGetStackHighWaterMark
        XString::number(list[i].xTaskNumber) + "\t" +
        XString::number(list[i].ulRunTimeCounter));

        printf("%s\n", str.data());
    }

    printf("%s\n", XString("\nFree heap: " + XString::number(xPortGetFreeHeapSize())).data());

    vPortFree(list);
  }

  return 0;
}
