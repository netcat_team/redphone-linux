/*
 * XDialer.cpp
 *
 *  Created on: Oct 28, 2017
 *      Author: netcat
 */

#include "xdialer.h"

#include "xlabel.h"
#include "xwindow.h"
#include "xpushbutton.h"
#include "xswitch.h"
#include "xslider.h"
#include "xtetrix.h"
#include "xsystem.h"

#define KEY_OFFSET_Y 80
#define KEY_OFFSET_X 20
#define KEY_SIZE 80
#define KEY_Y_POS(line) (KEY_OFFSET_Y + (line) * (KEY_SIZE + 10))
#define KEY_X_POS(column) (KEY_OFFSET_X + (column) * (KEY_SIZE + 10))
#define KEY_POS(column, line) KEY_X_POS(column), KEY_Y_POS(line)

XDialer::XDialer(XWidget *parent) : XWindow(parent),
  slNumClick(this, &XDialer::NumClick),
  slHideClick(this, &XDialer::HideClick),
  slCallClick(this, &XDialer::CallClick),
  slClearClick(this, &XDialer::ClearClick),
  slSystemTempExecClick(this, &XDialer::systemTempExecClick)
{
  setGeometry(XRect(70, 150, 300, 540));
  setWindowTitle("Dialer");
  buildIf();

  systemTempExec = new XPushButton(xSys()->topLevelWidget());
  systemTempExec->setGeometry(XRect(10, 730, 60, 60));
  systemTempExec->setText("Dial");
  systemTempExec->setFont("chococooky");
  systemTempExec->sReleased.connect(slSystemTempExecClick);

  hide();
}

void XDialer::systemTempExecClick(XWidget *)
{
  show();
}

void XDialer::NumClick(XWidget *w)
{
  XPushButton *pb = reinterpret_cast<XPushButton *>(w);
  if(pb->text() == "0" && mNumberLabel->text() == "0") mNumberLabel->setText("+");
  else mNumberLabel->setText(mNumberLabel->text() + pb->text());
}

void XDialer::HideClick(XWidget *w)
{
  hide();
}

void XDialer::CallClick(XWidget *w)
{
  if(mNumberLabel->text().isExist())
  {
    xSys()->modem()->call(mNumberLabel->text());
    mNumberLabel->setText("");
  }
}

void XDialer::ClearClick(XWidget *w)
{
  mNumberLabel->setText("");
}

void XDialer::buildIf()
{
  XSysGlMutex mutex;
  char sym[] = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#' };
  const char *fontName = "chococooky";

  mNumberLabel = new XLabel(this);
  mNumberLabel->setGeometry(XRect(20, 30, 210, KEY_SIZE / 2));
  mNumberLabel->setText("");
  mNumberLabel->setAlingent(AlignHCenter);
  mNumberLabel->setFont(fontName);

  for(uint32_t i = 0; i < sizeof(sym); i++)
  {
    pb[i] = new XPushButton(this);
    pb[i]->setGeometry(XRect(KEY_POS(i % 3, i / 3), KEY_SIZE, KEY_SIZE));
    pb[i]->setText(sym[i]);
    pb[i]->sReleased.connect(slNumClick);
    pb[i]->setFont(fontName);
  }

  pb[12] = new XPushButton(this);
  pb[12]->setGeometry(XRect(KEY_POS(0, 4), KEY_SIZE, KEY_SIZE));
  pb[12]->setText("Call");
  pb[12]->setFont(fontName);
  pb[12]->sReleased.connect(slCallClick);

  pb[13] = new XPushButton(this);
  pb[13]->setGeometry(XRect(KEY_POS(2, 4), KEY_SIZE, KEY_SIZE));
  pb[13]->setText("Hide");
  pb[13]->setFont(fontName);
  pb[13]->sReleased.connect(slHideClick);

  pb[14] = new XPushButton(this);
  pb[14]->setGeometry(XRect(KEY_X_POS(2) + 40, 30, KEY_SIZE - 40, KEY_SIZE / 2));
  pb[14]->setText("<-");
  pb[14]->setFont(fontName);
  pb[14]->sReleased.connect(slClearClick);
}

XDialer::~XDialer()
{

}

