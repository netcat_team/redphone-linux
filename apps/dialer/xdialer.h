/*
 * XDialer.h
 *
 *  Created on: Oct 28, 2017
 *      Author: netcat
 */

#ifndef APPS_DIALER_XDIALER_H_
#define APPS_DIALER_XDIALER_H_

#include "xobject.h"
#include "xwindow.h"

class XLabel;
class XPushButton;
class XWindow;
class XWidget;

class XDialer: public XWindow
{
public:
  XDialer(XWidget *parent = 0);
  virtual ~XDialer();

private:
  XLabel *mNumberLabel;
  XPushButton *pb[15];
  XPushButton *systemTempExec;

  void buildIf();

private slots:
  void NumClick(XWidget *);
  void HideClick(XWidget *w);
  void CallClick(XWidget *);
  void ClearClick(XWidget *);
  void systemTempExecClick(XWidget *);

  XSlot<XDialer, XWidget *> slNumClick;
  XSlot<XDialer, XWidget *> slHideClick;
  XSlot<XDialer, XWidget *> slCallClick;
  XSlot<XDialer, XWidget *> slClearClick;
  XSlot<XDialer, XWidget *> slSystemTempExecClick;
};

#endif /* APPS_DIALER_XDIALER_H_ */
