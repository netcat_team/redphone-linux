/*
 * XDialer.h
 *
 *  Created on: Oct 28, 2017
 *      Author: netcat
 */

#ifndef APPS_DIALER_XRINGDIALOG_H_
#define APPS_DIALER_XRINGDIALOG_H_

#include "xobject.h"

#include "xlabel.h"
#include "xwindow.h"
#include "xpushbutton.h"
#include "xswitch.h"
#include "xslider.h"
#include "xsystem.h"

class XRingDialog: public XWindow
{
public:
  XRingDialog(XWidget *parent = 0);
  virtual ~XRingDialog();

private:
  XLabel *mNumberLabel;
  XPushButton *pbAccept, *pbDecline;

  void buildIf();
  void configureIf(bool isIncomingCall);

private slots:
  void AcceptClick(XWidget *w);
  void DeclineClick(XWidget *w);
  void ring();
  void callEnd();
  void outgointCall();

  XSlot<XRingDialog, XWidget *> slAcceptClick;
  XSlot<XRingDialog, XWidget *> slDeclineClick;
  XSlot<XRingDialog> slRing;
  XSlot<XRingDialog> slCallEnd;
  XSlot<XRingDialog> slOutgointCall;
};

#endif /* APPS_DIALER_XRINGDIALOG_H_ */
