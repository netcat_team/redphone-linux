/*
 * XDialer.cpp
 *
 *  Created on: Oct 28, 2017
 *      Author: netcat
 */

#include "xringdialog.h"

const XWidgetStyle mWidgetStyle[] = {
  // green
  { xRgb(50, 210, 120), // borderColor
    xRgba(0, 0, 0, 190), // containerBgColor
    xRgba(100, 150, 100, 150), // containerFgColor
    xRgb(80, 100, 80), // componentBgColor
    xRgb(100, 150, 100), // componentBgTriggeredColor
    xRgb(140, 255, 140) }, // textColor
  // red
  { xRgb(210, 50, 120), // borderColor
    xRgba(0, 0, 0, 190), // containerBgColor
    xRgba(150, 100, 100, 150), // containerFgColor
    xRgb(100, 80, 80), // componentBgColor
    xRgb(150, 100, 100), // componentBgTriggeredColor
    xRgb(255, 140, 140) } // textColor
};

XRingDialog::XRingDialog(XWidget *parent) : XWindow(parent),
  slAcceptClick(this, &XRingDialog::AcceptClick),
  slDeclineClick(this, &XRingDialog::DeclineClick),
  slRing(this, &XRingDialog::ring),
  slCallEnd(this, &XRingDialog::callEnd),
  slOutgointCall(this, &XRingDialog::outgointCall)
{
  hide();
  setGeometry(XRect(70, 190, 350, 150));
  buildIf();

  xSys()->modem()->sRing.connect(slRing);
  xSys()->modem()->sCallEnd.connect(slCallEnd);
  xSys()->modem()->sOutgointCall.connect(slOutgointCall);
}

void XRingDialog::AcceptClick(XWidget *w)
{
  xSys()->modem()->pickUp();
}

void XRingDialog::DeclineClick(XWidget *w)
{
  xSys()->modem()->hungUp();
  hide();
}

void XRingDialog::ring()
{
  configureIf(true);
  show();
  setActive();
  mNumberLabel->setText(xSys()->modem()->number());
}

void XRingDialog::outgointCall()
{
  configureIf(false);
  show();
  setActive();
  mNumberLabel->setText(xSys()->modem()->number());
}

void XRingDialog::callEnd()
{
  hide();
  mNumberLabel->setText("");
}

void XRingDialog::configureIf(bool isIncomingCall)
{
  if(isIncomingCall)
  {
    pbAccept->show();
    pbDecline->move(XPoint(180, 80));
    setWindowTitle("Ring");
  }
  else
  {
    pbAccept->hide();
    pbDecline->move(XPoint(100, 80));
    setWindowTitle("Dialing");
  }
}

void XRingDialog::buildIf()
{
  XSysGlMutex mutex;

  mNumberLabel = new XLabel(this);
  mNumberLabel->setGeometry(XRect(20, 30, 290, 30));
  mNumberLabel->setText("");
  mNumberLabel->setFont("chococooky");

  pbAccept = new XPushButton(this);
  pbAccept->setGeometry(XRect(20, 80, 140, 40));
  pbAccept->setStyle(mWidgetStyle[0]);
  pbAccept->setText("Accept");
  pbAccept->setFont("chococooky");
  pbAccept->sReleased.connect(slAcceptClick);

  pbDecline = new XPushButton(this);
  pbDecline->setGeometry(XRect(180, 80, 140, 40));
  pbDecline->setStyle(mWidgetStyle[1]);
  pbDecline->setText("Decline");
  pbDecline->setFont("chococooky");
  pbDecline->sReleased.connect(slDeclineClick);
}

XRingDialog::~XRingDialog()
{

}
