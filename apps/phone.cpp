/*
 * phone.cpp
 *
 *  Created on: Nov 5, 2018
 *      Author: netcat
 */

#include <xsystem.h>

#include "dialer/xdialer.h"
#include "dialer/xringdialog.h"

SYS_APP(phone);

int phone_main(int argc, char **argv)
{
  new XDialer();
  new XRingDialog();

  return 0;
}
