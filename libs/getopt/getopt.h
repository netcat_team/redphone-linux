#ifndef _GETOPT_H_
#define _GETOPT_H_

typedef struct
{
    int ind;
    int opt;
    char *arg;
    int err;
} getopt_ctx_t;

void getopt_init(getopt_ctx_t *ctx);
int getopt(getopt_ctx_t *ctx, int argc, char *argv[], const char *optstring);

#endif

