/*
 * xpixmap.cpp
 *
 *  Created on: Nov 1, 2017
 *      Author: netcat
 */

#include "xpixmap.h"

#define ADR_FILE_HEADER 0x00
#define ADR_INFO_HEADER 0x0E

#define BMP_FILE_TYPE 0x4d42

extern "C" {
#include "libs/fatfs/fatio.h"
}

typedef struct __attribute__ ((packed))
{
  uint16_t type;
  uint32_t size;
  uint16_t reserved1;
  uint16_t reserved2;
  uint32_t offset_bit;
} bmp_file_headr_t;

typedef struct __attribute__ ((packed))
{
 uint32_t size;
 uint32_t width;
 uint32_t height;
 uint16_t planes;
 uint16_t bit_count;
 uint32_t compression;
 uint32_t size_img;
 uint32_t xpels_per_meter;
 uint32_t ypels_per_meter;
 uint32_t colors_used;
 uint32_t colors_important;
} bmp_info_t;

XPixmap::XPixmap() : mData(0), mSize(XSize(0, 0)), mBpp(0)
{

}

void XPixmap::loadFromData(const uint8_t *data, const XSize &size)
{
  delete mData;
  mData = new uint8_t[size.w() * size.h() * 4];
  mSize = size;
  uint32_t cnt = size.square();
  uint32_t *alloc = (uint32_t *)mData;

  while(cnt--) *alloc++ = ((*data++) << 24) | 0xbbbbff;
}

void XPixmap::load(const XString &fileName)
{
  // uint32_t w, h;
  fat_file_t *f = f_open((const char *)fileName.data());
  if(f)
  {
  	bmp_file_headr_t *header = (bmp_file_headr_t *)f->buf;
    bmp_info_t *info = (bmp_info_t *)&f->buf[sizeof(bmp_file_headr_t)];

    if(header->type != BMP_FILE_TYPE)
    {
      syslog(SYSLOG_ERROR, "it is not bmp file: %s", fileName.data());
      f_close(f);
      return;
    }

    delete mData;
  	mSize.resize(info->width, info->height);
  	uint32_t size = info->size_img;
    uint32_t bpp = info->bit_count >> 3;
    uint32_t palette_size = info->colors_used << 2;

    f_seek(f, sizeof(bmp_file_headr_t) + sizeof(bmp_info_t) + palette_size);

    if(bpp == 3)
    {
      mBpp = 4;
      uint32_t t_size = size;
      mData = new uint8_t[t_size * mBpp];

      uint32_t *buf = (uint32_t *)mData;
      while(t_size)
      {
        uint32_t t = 0;
        f_read(f, (uint8_t *)&t, 3);
        if(t) t |= 0xff000000;

        t_size -= 3;
        *buf++ = t;
      }
    }
    else if(bpp == 1)
    {
      mBpp = 1;
      mData = new uint8_t[size * mBpp];
      f_read(f, mData, size);
    }
    syslog(SYSLOG_DEBUG, "load bmp: %s, %d bytes", fileName.data(), size);

  	f_close(f);
  }
  else
  {
    syslog(SYSLOG_ERROR, "error open: %s", fileName.data());
  }
}

XPixmap::~XPixmap()
{
  delete mData;
}

