/*
 * xwidget.h
 *
 *  Created on: Jan 5, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XWIDGET_H_
#define GXLIB_XWIDGET_H_

#include "xobject.h"
#include "xrect.h"
#include "xpainter.h"
#include "xevent.h"

typedef struct
{
  uint32_t borderColor;
  uint32_t containerBgColor;
  uint32_t containerFgColor;
  uint32_t componentBgColor;
  uint32_t componentBgTriggeredColor;
  uint32_t textColor;
} XWidgetStyle;

class XWidget : public XObject
{
  friend class XSystem;

public:
  XWidget(XObject *parent = 0);
  virtual ~XWidget();

  bool isActive();
  void setActive();

  bool isVisible() const { return mVisible; }
  void hide() { mVisible = false; update(); }
  void show() { mVisible = true; update(); }

  const XRect& geometry() const { return mGeometry; }
  void setGeometry(const XRect& geometry);
  const XPoint& pos() const { return mGeometry.pos(); }
  const XSize& size() const { return mGeometry.size(); }

  void setStyle(const XWidgetStyle &style) { mStyle = &style; }
  void setFont(const XString &fontName);

  virtual void move(const XPoint& pos);
  virtual void resize(const XSize& size);

  XWidget *parentWidget() { return reinterpret_cast<XWidget *>(mParent); }

  virtual void update();

protected:
  bool mVisible;
  bool mEnabled;
  XRect mGeometry;
  const XWidgetStyle *mStyle;
  const XFont *mFont;

  // tmp varibles
  XRect mArea;
  XPoint mPos;

  void setWidgetOnTop();
  XPoint mapTo(XWidget *widget);
  XPoint mapToGlobal();

  virtual void paintEvent(XPainter *p) { }
  virtual void tsEvent(XTouchEvent *e) { }
  virtual void resizeEvent() { }
};

#endif
