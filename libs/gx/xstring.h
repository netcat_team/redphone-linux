/*
 * xstring.h
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XSTRING_H_
#define GXLIB_XSTRING_H_

extern "C" {
#include "sys/sys.h"
}

class XString
{
public:
  XString();
  XString(const char *str);
  XString(const XString &str);
  XString(const char *str, uint32_t size);
  XString(char c);
  virtual ~XString();

  XString &operator+=(const XString &str);
  XString &operator=(const XString &str);
  bool operator==(const XString &s1) const;
  
  const char *data() const { return mData; }
  char *data() { return mData; }
  uint32_t lenght() const { return mSize; }
  
  bool isEmpty() const { return !mSize; }
  bool isExist() const { return mSize; }
  void chop(uint32_t n);
  void truncate(uint32_t pos);
  XString left(uint32_t n) const;
  XString right(uint32_t n) const;
  void fill(char ch, uint32_t size = -1);

  static XString number(int32_t n, uint32_t base = 16);
  
private:
  uint32_t mSize;
  char *mData;
};

inline const XString operator+(const XString &s1, const XString &s2)
    { XString t(s1); t += s2; return t; }
inline const XString operator+(const XString &s1, const char *s2)
    { XString t(s1); t += s2; return t; }
inline const XString operator+(const char *s1, const XString &s2)
    { XString t(s1); t += s2; return t; }
    
#endif /* GXLIB_XSTRING_H_ */
