/*
 * xlabel.h
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XLABEL_H_
#define GXLIB_WIDGETS_XLABEL_H_

#include "xwidget.h"
#include "xsigslot.h"

class XLabel: public XWidget
{
public:
  XLabel(XObject *parent = 0);
  virtual ~XLabel() {};

  void setText(const XString& text);
  const XString& text() const { return mText; }
  void setAlingent(AlignmentFlag alingment) { mAlignment = alingment; }

  XSignal<> sClicked;

protected:
  XString mText;
  AlignmentFlag mAlignment;

  virtual void paintEvent(XPainter *p);
};

#endif /* GXLIB_WIDGETS_XLABEL_H_ */
