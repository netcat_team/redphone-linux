/*
 * xlabel.cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#include "xlabel.h"
#include "xsystem.h"

XLabel::XLabel(XObject *parent) : XWidget(parent), mAlignment(NoAlign)
{
}

void XLabel::paintEvent(XPainter *p)
{
  p->setPen(xRgb(255, 255, 255));
  uint32_t x = (mAlignment & AlignHCenter) ? size().w() >> 1 : 0;
  uint32_t y = (mAlignment & AlignVCenter) ? size().h() >> 1 : 0;
  p->drawText(XPoint(x, y), mText, mFont, mAlignment);
}

void XLabel::setText(const XString& text)
{
  mText = text;
  update();
}
