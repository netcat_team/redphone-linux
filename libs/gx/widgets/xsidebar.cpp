/*
 * xsidebar.cpp
 *
 *  Created on: Nov 6, 2016
 *      Author: netcat
 */

#include "xsidebar.h"
#include "xsystem.h"

#define HEIGHT 40

XSideBar::XSideBar(XWidget *parent) : XWidget(parent)
{
  resize(XSize(parent->size().w(), HEIGHT));
  mMove = false;
  mIsOpen = false;
}

void XSideBar::paintEvent(XPainter *p)
{
  if(mIsOpen)
  {
	  p->setBrush(xRgba(0, 0, 0, 200));
	  p->fillRect(XRect(0, 0, size().w(), size().h()));
	  p->setBrush(xRgb(0, 0, 0));
	  p->drawLine(XPoint(0, size().h()), XPoint(size().w(), size().h()));
  }
  else
  {
    p->setBrush(xRgba(0, 0, 0, 150));
	  p->fillRect(XRect(0, 0, size().w(), size().h()));
	  p->setBrush(xRgb(0, 0, 0));
	  p->drawLine(XPoint(0, size().h()), XPoint(size().w(), size().h()));
  }
}

void XSideBar::tsEvent(XTouchEvent *e)
{
  e->accept();
  uint32_t y = e->point().y();

  switch(e->status())
  {
  case TS_STATUS_PRESSED:
//    if(XRect(0, 0, size().w(), HEIGHT).inside(e->point()))
//    {
      mMove = true;
      mIsOpen = true;
      update();
//    }
    break;

  case TS_STATUS_MOVED:
  {
    if(mMove)
    {
      if(y < HEIGHT) y = HEIGHT;
      resize(XSize(size().w(), y));
    }
  }
  break;

  case TS_STATUS_RELEASED:
    if(y > 400) y = 800;
    else
    {
      y = HEIGHT;
      mIsOpen = false;
    }
    mMove = false;
    resize(XSize(size().w(), y));
    break;
  }
}

XSideBar::~XSideBar()
{

}
