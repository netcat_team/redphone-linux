/*
 * xsideswitch.cpp
 *
 *  Created on: Feb 11, 2016
 *      Author: netcat
 */

#include "xswitch.h"

#include "xsystem.h"

XSwitch::XSwitch(XWidget *parent)
  : XAbstractButton(parent)
{
  mCheckable = true;
}

XSwitch::~XSwitch()
{
}

void XSwitch::paintEvent(XPainter *p)
{
  uint32_t len = INT_MUL16(size().w(), 0.4);

  p->setPen(xRgb(150, 150, 150));

  if(mChecked) p->setBrush(xRgb(30, 30, 30));
  else p->setBrush(xRgb(30, 30, 30));

  p->drawRect(XRect(0, 0, size().w() - 1, size().h() - 1));

  p->setPen(xRgb(50, 100, 250));
  p->setBrush(xRgb(20, 80, 130));
  if(mChecked)
  {
    p->drawRect(XRect(size().w() - len + 2, 2, len - 5, size().h() - 5));
    p->setPen(xRgb(50, 250, 100));
    p->drawText(XPoint(len - 5, size().h() >> 1), "on", mFont, AlignCenter);
  }
  else
  {
    p->drawRect(XRect(2, 2, len - 5, size().h() - 5));
    p->setPen(xRgb(250, 100, 100));
    p->drawText(XPoint(size().w() - len + 2, size().h() >> 1), "off", mFont, AlignCenter);
  }
}

void XSwitch::tsEvent(XTouchEvent *e)
{
  e->accept();

  switch(e->status())
  {
    case TS_STATUS_PRESSED:
      mChecked = !mChecked;
      sReleased(this);
      update();
      break;

    case TS_STATUS_MOVED:
    case TS_STATUS_RELEASED:
      break;
  }
}

