/*
 * xpushbutton.cpp
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#include "xpushbutton.h"
#include "xsystem.h"

XPushButton::XPushButton(XWidget *parent) : XAbstractButton(parent)
{
}

XPushButton::~XPushButton()
{
}

void XPushButton::paintEvent(XPainter *p)
{
  p->setPen(mStyle->borderColor);

  if(mChecked)
  {
    p->setBrush(xRgba(255, 255, 255, 0));
    p->drawRect(XRect(0, 0, size().w() - 2, size().h() - 2));

    p->setBrush(mStyle->componentBgTriggeredColor);
    p->drawRect(XRect(2, 2, size().w() - 6, size().h() - 6));
  }
  else
  {
    p->setBrush(mStyle->componentBgColor);
    p->drawRect(XRect(0, 0, size().w() - 2, size().h() - 2));
  }

  // p->setPen(xRgb(255, 255, 255));
  // p->drawText(geometry().center(), mText, AlignCenter);
  p->setPen(mStyle->textColor);
  p->drawText(geometry().center(), mText, mFont, AlignCenter);
}

void XPushButton::tsEvent(XTouchEvent *e)
{
  e->accept();

  switch(e->status())
  {
    case TS_STATUS_PRESSED:
      mChecked = true;
      update();
      break;

    case TS_STATUS_MOVED:
      break;

    case TS_STATUS_RELEASED:
      mChecked = false;
      if(XRect(size()).inside(e->point())) sReleased(this);
      update();
      break;
  }
}
