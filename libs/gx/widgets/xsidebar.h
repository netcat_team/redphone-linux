/*
 * xsidebar.h
 *
 *  Created on: Nov 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XSIDEBAR_H_
#define GXLIB_WIDGETS_XSIDEBAR_H_

#include "xwidget.h"

class XSideBar: public XWidget
{
public:
  XSideBar(XWidget *parent);
  virtual ~XSideBar();

protected:
  virtual void paintEvent(XPainter *p);
  virtual void tsEvent(XTouchEvent *e);
  // virtual void resizeEvent();

private:
  XPoint mTouchPoint;
  bool mMove;
  bool mIsOpen;
};

#endif /* GXLIB_WIDGETS_XSIDEBAR_H_ */
