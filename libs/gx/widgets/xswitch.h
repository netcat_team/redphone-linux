/*
 * xsideswitch.h
 *
 *  Created on: Feb 11, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XSWITCH_H_
#define GXLIB_WIDGETS_XSWITCH_H_

#include "xabstractbutton.h"

class XSwitch: public XAbstractButton
{
public:
  XSwitch(XWidget *parent = 0);
  virtual ~XSwitch();

protected:
  virtual void paintEvent(XPainter *p);
  virtual void tsEvent(XTouchEvent *e);
};

#endif /* GXLIB_WIDGETS_XSWITCH_H_ */
