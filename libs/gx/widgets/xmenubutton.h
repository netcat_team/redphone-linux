/*
 * xmenubutton.h
 *
 *  Created on: Nov 9, 2017
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XMENUBUTTON_H_
#define GXLIB_WIDGETS_XMENUBUTTON_H_

#include "xabstractbutton.h"

class XMenuButton: public XAbstractButton
{
public:
  XMenuButton(XWidget *parent = 0);
  virtual ~XMenuButton();

protected:
  virtual void paintEvent(XPainter *p);
  virtual void tsEvent(XTouchEvent *e);
};

#endif /* GXLIB_WIDGETS_XMENUBUTTON_H_ */
