/*
 * xmenubutton.cpp
 *
 *  Created on: Nov 9, 2017
 *      Author: netcat
 */

#include "xmenubutton.h"
#include "xsystem.h"

XMenuButton::XMenuButton(XWidget *parent) : XAbstractButton(parent)
{
}

XMenuButton::~XMenuButton()
{
}

void XMenuButton::paintEvent(XPainter *p)
{
  p->setPen(xRgb(50, 120, 210));

  if(mChecked)
  {
    p->setBrush(xRgba(255, 255, 255, 0));
    p->drawRect(XRect(0, 0, size().w() - 2, size().h() - 2));

    p->setBrush(xRgb(100, 100, 150));
    p->drawRect(XRect(2, 2, size().w() - 6, size().h() - 6));
  }
  else
  {
    p->setBrush(xRgb(80, 80, 100));
    p->drawRect(XRect(0, 0, size().w() - 2, size().h() - 2));
  }

  p->setPen(xRgb(255, 255, 255));
  p->drawText(geometry().center(), mText, mFont, AlignCenter);
}

void XMenuButton::tsEvent(XTouchEvent *e)
{
  e->accept();

  switch(e->status())
  {
    case TS_STATUS_PRESSED:
      mChecked = true;
      update();
      break;

    case TS_STATUS_MOVED:
      break;

    case TS_STATUS_RELEASED:
      mChecked = false;
      if(XRect(size()).inside(e->point())) sReleased(this);
      update();
      break;
  }
}
