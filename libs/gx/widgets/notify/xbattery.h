/*
 * xbattery.h
 *
 *  Created on: Nov 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XBATTERY_H_
#define GXLIB_WIDGETS_XBATTERY_H_

#include "xwidget.h"

class XBattery: public XWidget
{
public:
  XBattery(XWidget *parent);
  virtual ~XBattery();
  void setValue(float value);
  float value() const { return mValue; }

protected:
  virtual void paintEvent(XPainter *p);

private:
  float mValue;
};

#endif /* GXLIB_WIDGETS_XBATTERY_H_ */
