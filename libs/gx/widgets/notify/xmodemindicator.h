/*
 * xmodemindicator.h
 *
 *  Created on: Nov 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_WIDGETS_XMODEMINDICATOR_H_
#define GXLIB_WIDGETS_XMODEMINDICATOR_H_

#include "xwidget.h"

class XModemIndicator: public XWidget
{
public:
  XModemIndicator(XWidget *parent);
  virtual ~XModemIndicator();
  void setValue(uint32_t value);
  uint32_t value() const { return mValue; }

protected:
  virtual void paintEvent(XPainter *p);
  // virtual void tsEvent(XTouchEvent *e);
  // virtual void resizeEvent();

private:
  uint32_t mValue;
};

#endif /* GXLIB_WIDGETS_XMODEMINDICATOR_H_ */
