/*
 * xmodemindicator.cpp
 *
 *  Created on: Nov 6, 2016
 *      Author: netcat
 */

#include "xmodemindicator.h"
#include "xsystem.h"

#define WIDTH 50
#define HEIGHT 28
#define NUM 7
#define SPACE 1

#define BG_CLR xRgba(255, 120, 120, 255)

#define EMERG_CLR xRgb(149, 64, 64)
#define FILL_CLR xRgb(149, 149, 149)
#define BORDER_CLR xRgb(64, 64, 64)

XModemIndicator::XModemIndicator(XWidget *parent) : XWidget(parent), mValue(0)
{
  move(XPoint(480 - (WIDTH - SPACE * 2) - 10, 0));
  resize(XSize(WIDTH - SPACE * 2, 40));
}

void XModemIndicator::setValue(uint32_t value)
{
  if(value > NUM) mValue = NUM;
  else mValue = value;
  update();
}

void XModemIndicator::paintEvent(XPainter *p)
{ 
  // p->setBrush(0);
  //p->drawRect(XRect(0, 0, size().w() - 1, size().h() - 1));

  // XPixmap ico;
  // ico.load("/sys/media/2.bmp");
  // p->drawPixmap(XPoint(0, 0), ico);

  uint32_t dx = (WIDTH - (NUM + 1) * SPACE) / NUM;
  uint32_t dy = HEIGHT / NUM;
  int32_t x = (size().w() - WIDTH) >> 1;
  int32_t y = (size().h() - HEIGHT) >> 1;

  //p->drawRect(XRect(x, y, WIDTH, HEIGHT));

  p->setBrush(BG_CLR);
  for(uint32_t i = 0; i < NUM; i++)
  {
    if(mValue) p->setPen(FILL_CLR);
    else p->setPen(EMERG_CLR);

    if(i < mValue) p->setBrush(FILL_CLR);
    else  p->setBrush(BORDER_CLR);
    p->drawRect(XRect(SPACE + x + i * (dx + SPACE), y + ((NUM - 1) - i) * dy, dx - 1, dy * (i + 1) - 1));
  }
}

XModemIndicator::~XModemIndicator()
{
}
