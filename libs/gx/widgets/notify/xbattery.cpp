/*
 * xbattery.cpp
 *
 *  Created on: Nov 6, 2016
 *      Author: netcat
 */

#include "xbattery.h"
#include "xsystem.h"

#define WIDTH 18
#define HEIGHT 28
#define HEIGHT_P 3
#define BG_CLR xRgb(84, 84, 84)
#define FG_CLR xRgb(142, 204, 1)

XBattery::XBattery(XWidget *parent) : XWidget(parent), mValue(0.2)
{
  move(XPoint(480 - 90, 0));
  resize(XSize(30, 40));
}

void XBattery::setValue(float value)
{
  if(value > 1.0) mValue = 1;
  else if(value < 0) mValue = 0;
  else mValue = value;
  update();
}

void XBattery::paintEvent(XPainter *p)
{
  p->setPen(xRgba(255, 0, 0, 90));
  p->setBrush(0);
  //p->drawRect(XRect(0, 0, size().w() - 1, size().h() - 1));

  int32_t x = (size().w() - WIDTH) >> 1;
  int32_t y = (size().h() - HEIGHT) >> 1;
  uint32_t size_fix = (HEIGHT - HEIGHT_P - 2) * mValue;
  uint32_t offset_fix = (HEIGHT - HEIGHT_P - 2) - size_fix;

  //p->drawRect(XRect(x, y, WIDTH, HEIGHT));

  p->setBrush(BG_CLR);
  p->fillRect(XRect(x, y + HEIGHT_P, WIDTH, HEIGHT - HEIGHT_P));
  p->fillRect(XRect(x + ((WIDTH >> 1) - 3), y, 6, HEIGHT_P));

  p->setBrush(FG_CLR);
  p->fillRect(XRect(x + 1, y + 4 + offset_fix, WIDTH - 2, size_fix));
}

XBattery::~XBattery()
{
}
