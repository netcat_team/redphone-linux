#include "xwindow.h"
#include "xsystem.h"

#define WIN_HEADER_SIZE 15

XWindow::XWindow(XWidget *parent)
    : XWidget(parent), mMove(false)
{
}

void XWindow::paintEvent(XPainter *p)
{
  p->setPen(mStyle->borderColor);
  p->setBrush(mStyle->containerBgColor);
  p->drawRect(XRect(0, 0, size().w() - 1, size().h() - 1));

  p->setBrush(mStyle->containerFgColor);
  p->drawRect(XRect(0, 0, size().w() - 1, WIN_HEADER_SIZE));

  p->setPen(mStyle->textColor);
  p->drawText(XPoint(5, 5), mTitle, mFont);
}

void XWindow::tsEvent(XTouchEvent *e)
{
  e->accept();

  switch(e->status())
  {
  case TS_STATUS_PRESSED:
    if(XRect(0, 0, size().w(), WIN_HEADER_SIZE).inside(e->point()))
    {
      mMove = true;
      mTouchPoint = e->point();
      update();
    }
    break;

  case TS_STATUS_MOVED:
    if(mMove) move(pos() + e->point() - mTouchPoint);
    break;

  case TS_STATUS_RELEASED:
    mMove = false;
    break;
  }
}

void XWindow::setWindowTitle(const XString& text)
{
  mTitle = text;
  update();
}

//void XWindow::closeButtonClick()
//{
//    closeEvent();
//    hide();
//}
