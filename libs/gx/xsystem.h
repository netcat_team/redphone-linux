/*
 * xsystem.h
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XSYSTEM_H_
#define GXLIB_XSYSTEM_H_

#include "xobject.h"
#include "xwidget.h"
#include "xscreen.h"
#include "xtouch.h"
#include "xstring.h"
#include "xgsmservice.h"
#include "xsidebar.h"
#include "notify/xbattery.h"
#include "notify/xmodemindicator.h"

#include "fonts/xfontlist.h"

extern "C" {
#include "sys/sys.h"
}

#define INT_MUL16(n, div) (((n) * (int)((div) * (1 << 16))) >> 16)

class XSystem : public XObject
{
  friend class XTouch;
  friend class XSysGlMutex;
  friend class XPainter;
  friend class XGsmService;

public:
  static XSystem *instance()
  {
    if(!mSelf)
    {
      syslog(SYSLOG_DEBUG, "xstart");
      mSelf = new XSystem();
      mSelf->init();
    }
    return mSelf;
  }

  void redraw();
  const XWidgetStyle &style() const;
  const XFont *font(const XString &fontName) const { return mFontList.find(fontName); }
  const XFont *font() const { return mFontList.std(); }
  XWidget *topLevelWidget() { return mTopLevelWidget; }
  XGsmService *modem() { return mGsmService; }
  XTouch *ts() { return mTouch; }

  XTouch *mTouch;
  XGsmService *mGsmService;

private:
  static XSystem *mSelf;
  XScreen *mScreen;
  XPainter *mPainter;
  XWidget *mTopLevelWidget;
  XWidget *mLastFocus;
  XPixmap *mWallpaper;
  XSideBar *mSideBar;
  XBattery *mBattery;
  XModemIndicator *mModemIndicator;

  int mGlLockCnt;
  XFontList mFontList;

  XSystem() : XObject(), mTouch(0), mGsmService(0), mScreen(0), mPainter(0), mTopLevelWidget(0),
    mLastFocus(0), mWallpaper(0), mSideBar(0), mBattery(0), mModemIndicator(0), mGlLockCnt(0) {}
  void init();
  bool paintWidget(XObject *obj);
  void goTreeReverse(XObject *w, bool (XSystem::*action)(XObject *obj));
  void deliverTouch(XTouchEvent *e);
  void goTreeForward(XTouchEvent *e, XObject *w, bool (XSystem::*action)(XTouchEvent *e, XObject *obj), bool (XSystem::*action2)(XTouchEvent *e, XObject *obj));
  bool touchWidget(XTouchEvent *e, XObject *obj);
  bool touchIf(XTouchEvent *e, XObject *obj);
};

inline XSystem *xSys() { return XSystem::instance(); }

class XSysGlMutex
{
public:
  XSysGlMutex()
  {
//    xSys()->syslog(" > new mutex");
    xSys()->mGlLockCnt++;
  }
  ~XSysGlMutex()
  {
    xSys()->mGlLockCnt--;
//    xSys()->syslog(" > del mutex");
    xSys()->redraw();
  }
};

#endif /* GXLIB_XSYSTEM_H_ */
