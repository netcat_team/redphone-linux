/*
 * xtouch.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: netcat
 */

#include "xtouch.h"
#include "xsystem.h"

extern "C" {
#include "ts.h"
}

//XSystem *mSys;

XTouch::XTouch(XObject *obj = 0) : XObject(obj), mLastActiveFlag(false)
{
//  mSys = xSys();
}

XTouch::~XTouch()
{

}

void XTouch::update()
{
  XTouchEvent e;
  bsp_ts_state_t ts = bsp_ts_get();
  
  if(ts.st)
  {
    if(mLastActiveFlag)
    {
      e.mGlobal.move(ts.x, ts.y);
      e.mStatus = TS_STATUS_MOVED;
    }
    else
    {
      mLastActiveFlag = true;

      e.mGlobal.move(ts.x, ts.y);
      e.mStatus = TS_STATUS_PRESSED;
    }

    xSys()->deliverTouch(&e);
  }
  else
  {
    if(mLastActiveFlag)
    {
      mLastActiveFlag = false;

      e.mGlobal.move(ts.x, ts.y);
      e.mStatus = TS_STATUS_RELEASED;

      xSys()->deliverTouch(&e);
    }
  }
}
