/*
 * xgsmservice.h
 *
 *  Created on: Oct 29, 2017
 *      Author: netcat
 */

#ifndef GXLIB_XGSMSERVICE_H_
#define GXLIB_XGSMSERVICE_H_

#include "xobject.h"
#include "xstring.h"

class XGsmService: public XObject
{
public:
  XGsmService(XObject *obj = 0);
  virtual ~XGsmService();

  void pollIpc();
  void status();
  void setEnabled(bool enabled);
  void call(const XString &number);
  void hungUp();
  void pickUp();
  const XString &number() const { return mNumber; };

  XSignal<int> sRssiUpdate;
  XSignal<int> sConnectionStatusUpdate;
  XSignal<> sRing;
  XSignal<> sCallEnd;
  XSignal<> sOutgointCall;

private:
  XString mNumber;
  bool mFirsConnect;
};

#endif /* GXLIB_XGSMSERVICE_H_ */
