#ifndef XSIGNAL_H
#define XSIGNAL_H

#include "xlist.h"

#define slots

template<class... Types>
class XSlotBase
{
  public:
    virtual ~XSlotBase() {}
    virtual void run(Types... arg...) const = 0;
};

template<class T, class... Types>
class XSlot : public XSlotBase<Types...>
{
public:
  typedef void (T::*MemberFunPtr)(Types...);
  XSlot(T *objPtr, MemberFunPtr funPtr) : mObjPtr(objPtr), mFunPtr(funPtr) {}
  virtual void run (Types... arg...) const { (mObjPtr->*mFunPtr)(arg...); }
  virtual ~XSlot() {}

private:
  T *mObjPtr;
  MemberFunPtr mFunPtr;
};

template<class... Types>
class XSignal
{
public:
  XSignal() { mSlots.clear(); mSignals.clear(); }
  ~XSignal() { mSlots.clear(); mSignals.clear(); }

  template<class T>
  void connect(XSlot<T, Types...>& slot) { mSlots.add(&slot); }
  void connect(XSignal<Types...>& signal) { mSignals.add(&signal); }

  template<class T>
  void disconnect(const XSlot<T, Types...>& slot) { mSlots.remove(&slot); }
  void disconnect(const XSignal<Types...>& signal) { mSignals.remove(&signal); }

  void operator()(Types... arg...)
  {
    XSlotBase<Types... > *sl = mSlots.first();
    while(sl)
    {
      sl->run(arg...);
      sl = mSlots.next();
    }

    XSignal<Types...> *s = mSignals.first();
    while(s)
    {
      s->operator()(arg...);
      s = mSignals.next();
    }
  }

  void disconnectAll() { mSignals.clear(); mSlots.clear(); }
  void disconnectSignals() { mSignals.clear(); }
  void disconnectSlots() { mSlots.clear(); }

private:
  XList<XSlotBase<Types...> *> mSlots;
  XList<XSignal<Types...> *> mSignals;
};

#endif // XSIGNAL_H
