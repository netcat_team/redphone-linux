/*
 * xgsmservice.cpp
 *
 *  Created on: Oct 29, 2017
 *      Author: netcat
 */

#include "xgsmservice.h"
#include "xsystem.h"
extern "C" {
#include "sys/services/ipc.h"
#include "sys/services/syslog.h"
}

XGsmService::XGsmService(XObject *obj) : XObject(obj), mFirsConnect(true)
{
  ipc_init(IPC_GSM_SRV);

  if(gsm_init())
  {
    syslog(SYSLOG_ERROR, "GSM tty err!");
  }
}

void XGsmService::pollIpc()
{
  ipc_msg_t msg;
  if(ipc_poll(IPC_GSM_SRV, &msg))
  {
    switch(msg.type)
    {
      case GSM_DRIVER_MSG_RING:
        sRing();
        break;

      case GSM_DRIVER_MSG_POWER_DOWN:
        xSys()->mModemIndicator->setValue(0);
        if(mFirsConnect)
        {
          mFirsConnect = false;
          syslog(SYSLOG_DEBUG, "auto power up");
          setEnabled(true);
        }
        break;

      case GSM_DRIVER_MSG_RSSI:
        xSys()->mModemIndicator->setValue(msg.arg >> 2);
        break;

      case GSM_DRIVER_MSG_AOH:
        mNumber = XString((char*)msg.data, msg.size);
        vPortFree(msg.data); // TODO: check!
        sRing();
        break;

      case GSM_DRIVER_MSG_BUSY:
      case GSM_DRIVER_MSG_NO_ANSWER:
        sCallEnd();
        break;

      case GSM_DRIVER_MSG_OUTGOING_CALL:
        sOutgointCall();
        break;

      default:
        break;
    }
  }
}

void XGsmService::status()
{
  ipc_send(IPC_GSM_SRV, IPC_GSM_DRIVER, GSM_STATE_GET_SIM_ST, 0, 0, 0);
}

void XGsmService::setEnabled(bool enabled)
{
  ipc_send(IPC_GSM_SRV, IPC_GSM_DRIVER, enabled ? GSM_STATE_PWR_UP : GSM_STATE_PWR_DOWN, 0, 0, 0);
}

void XGsmService::call(const XString &number)
{
//  xSys()->syslog("call: " + number);
  mNumber = number;
  ipc_send(IPC_GSM_SRV, IPC_GSM_DRIVER, GSM_STATE_OUTGOING_CALL, 0, number.data(), number.lenght() + 1);
}

void XGsmService::hungUp()
{
  ipc_send(IPC_GSM_SRV, IPC_GSM_DRIVER, GSM_STATE_HANG_UP, 0, 0, 0);
}

void XGsmService::pickUp()
{
  ipc_send(IPC_GSM_SRV, IPC_GSM_DRIVER, GSM_STATE_PICK_UP, 0, 0, 0);
}

XGsmService::~XGsmService()
{
}

