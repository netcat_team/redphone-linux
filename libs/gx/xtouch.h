/*
 * xtouch.h
 *
 *  Created on: Feb 1, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XTOUCH_H_
#define GXLIB_XTOUCH_H_

#include "xobject.h"
#include "xscreen.h"

class XTouch: public XObject
{
public:
  XTouch(XObject *obj);
  virtual ~XTouch();
  void update();

private:
  bool mLastActiveFlag;
};

#endif /* GXLIB_XTOUCH_H_ */
