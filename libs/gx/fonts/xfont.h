/*
 * xfont.h
 *
 *  Created on: Nov 14, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XFONT_H_
#define GXLIB_XFONT_H_

#include "xsize.h"
#include "xstring.h"
#include "xpixmap.h"

class XFont
{
public:
  XFont(const XString &name, const uint8_t *data, const XSize &size, uint32_t st, uint32_t end, uint32_t bpp);
  XFont(const XString &name);
  virtual ~XFont();

  const uint8_t *data() const { return mData; }
  const XSize& size() const { return mSize; }
  uint32_t bpp() const { return mBpp; }
  const bool isMonospace() const { return mMonospace; }
  const XString &name() const { return mName; }
  const uint8_t *widthData() const { return mWidthData; }
  void load(const XString &fileName);

private:
  XString mName;
  const uint8_t *mData;
  const uint8_t *mWidthData;
  XSize mSize;
  uint32_t mSt, mEnd;
  uint32_t mBpp;
  bool mMonospace;


//  XPixmap *ico;
};

#endif /* GXLIB_XFONT_H_ */

