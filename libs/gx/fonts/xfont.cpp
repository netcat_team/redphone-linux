/*
 * xfont.cpp
 *
 *  Created on: Nov 14, 2016
 *      Author: netcat
 */

#include "xfont.h"
#include "sys/sys.h"

#define GFF_SIGNATURE 0x00464647

typedef struct __attribute__ ((packed))
{
  uint32_t signature;
  uint8_t name[32];
  uint8_t size;
  uint8_t code_st;
  uint8_t code_end;
  uint8_t bpp;
  uint16_t width;
  uint16_t height;
  uint32_t flags;
  uint32_t data_table_offset;
  uint32_t w_table_offset;
  uint32_t reserved[2];
} font_file_header_t;

XFont::XFont(const XString &name, const uint8_t *data, const XSize &size,
  uint32_t st, uint32_t end, uint32_t bpp) :
  mName(name), mData(data), mWidthData(0), mSize(size), mSt(st), mEnd(end), mBpp(bpp), mMonospace(true)
{

};

XFont::XFont(const XString &fileName) :
  mData(0), mWidthData(0), mSt(0), mEnd(0), mBpp(0), mMonospace(false)
{
  load(fileName);
};

void XFont::load(const XString &fileName)
{
  fat_file_t *f = f_open((const char *)fileName.data());
  if(f)
  {
    font_file_header_t *header = (font_file_header_t *)f->buf;

    if(header->signature != GFF_SIGNATURE)
    {
      f_close(f);
      return;
    }

    delete mData;
    delete mWidthData;

    mName = XString((const char *)header->name);
    syslog(SYSLOG_DEBUG, "load font: %s", (const char *)header->name);
    mSt = header->code_st;
    mEnd = header->code_end;
    mBpp = header->bpp;
    mSize = XSize(header->width, header->height);
    mMonospace = (header->flags & 1) ? true : false;

    uint32_t data_table_offset = header->data_table_offset;
    uint32_t w_table_offset = header->w_table_offset;
    uint32_t w_size = mEnd - mSt;
    uint32_t data_size = mSize.square() * w_size;
    
    uint8_t *data = new uint8_t[data_size];
    f_seek(f, data_table_offset);
    f_read(f, data, data_size);
    mData = data;

    uint8_t *w  = new uint8_t[w_size];
    f_seek(f, w_table_offset);
    f_read(f, w, w_size);
    mWidthData = w;

    f_close(f);
  }
}

XFont::~XFont()
{
  
}
