/*
 * xfontlist.h
 *
 *  Created on: Dec 3, 2017
 *      Author: netcat
 */

#ifndef GXLIB_FONTS_XFONTLIST_H_
#define GXLIB_FONTS_XFONTLIST_H_

#include "fonts/xfont.h"
#include "xstring.h"

class XFontList
{
public:
  XFontList();
  ~XFontList();
  void load(const XString& str);
  const XFont *std() const { return mFontStd; }
  const XFont *find(const XString& str) const;

private:
  XFont *mFontStd;
  XList<const XFont *> mList;
};

#endif /* GXLIB_FONTS_XFONTLIST_H_ */
