/*
 * xfontlist.cpp
 *
 *  Created on: Dec 3, 2017
 *      Author: netcat
 */

#include "xfontlist.h"
#include "fonts/build_in_font.h"

XFontList::XFontList()
{
  mFontStd = new XFont("std", font_raster5x8, XSize(5, 8), 0x20, 0x7a, 1);
  mList.push_back(mFontStd);
}

void XFontList::load(const XString& str)
{
  mList.push_back(new XFont(str));
}

const XFont *XFontList::find(const XString& str) const
{
  const XListNode<const XFont *> *fnt = mList.first();

  while(fnt)
  {
//    printf("name: %s \n", fnt->obj()->name().data());
    if(fnt->obj()->name() == str)
    {
//      printf("found!\n");
      return fnt->obj();
    }
    fnt = fnt->next();
  }
  return mFontStd;
}

XFontList::~XFontList()
{

}

