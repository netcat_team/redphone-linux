/*
 * xscreen.h
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XSCREEN_H_
#define GXLIB_XSCREEN_H_

#include "xobject.h"
#include "xsize.h"

typedef enum
{
  SCR_PORTAIN,
  SCR_LANDSCAPE,
} screen_orientation_e;

class XScreen : public XObject
{
public:
  XScreen(XObject *obj);
  virtual ~XScreen() {}
  const XSize& size() const { return mSize; }
  uint8_t *buf() { return mBuf; }
  void push();

private:
  screen_orientation_e mOr;
  XSize mSize;
  uint8_t *mBuf;
};

#endif /* GXLIB_XSCREEN_H_ */

