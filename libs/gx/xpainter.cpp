/*
 * xpainter.cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#include "xpainter.h"
#include "xsystem.h"
extern "C" {
#include "gma.h"
}

#define abs(n) ((n) > 0 ? (n) : (-n))

uint8_t frame_buf[LCD_SIZE * LCD_COLOR_SIZE];

int32_t vbuf_minx = 0;
int32_t vbuf_miny = 0;
int32_t vbuf_maxx = 0;
int32_t vbuf_maxy = 0;

int32_t sy = 0;
int32_t sx = 0;

void painter_copy_rect(uint32_t x, uint32_t y, uint32_t w, uint32_t h);
void painter_fill_rect(int32_t x, int32_t y, int32_t w, int32_t h);
void painter_clear();
void painter_draw_fast_vline(int32_t x, int32_t y, int32_t h);
void painter_draw_fast_hline(int32_t x, int32_t y, int32_t w);
void painter_draw_rect(int32_t x, int32_t y, uint32_t w, uint32_t h);
void painter_rect(int32_t x, int32_t y, int32_t w, int32_t h);
void painter_line(int32_t x0, int32_t y0, int32_t x1, int32_t y1);
void painter_fill_triangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2);
void painter_offset_scr(int32_t x, int32_t y);
void painter_window(uint32_t x, uint32_t y, uint32_t w, uint32_t h);
void painter_draw_circle(int32_t x, int32_t y, uint32_t r);
void painter_fill_circle(int32_t x, int32_t y, uint32_t r);
void painter_copy_layer_rect(uint32_t vbuf_to, uint32_t vbuf_from, uint32_t x, uint32_t y, uint32_t w, uint32_t h);

XPainter::XPainter(XObject *obj, uint8_t *buf) : XObject(obj),
  mBuf(buf), mPen(0), mBrush(0)
{

}

void XPainter::drawRect(const XRect& rect)
{
  painter_set_color(mPen);
  painter_draw_rect(rect.pos().x(), rect.pos().y(), rect.size().w(), rect.size().h());
  painter_set_color(mBrush);
  painter_fill_rect(rect.pos().x() + 1, rect.pos().y() + 1, rect.size().w() - 1, rect.size().h() - 1);
}

void XPainter::drawEllips(const XRect& rect)
{
  if(rect.isSquare())
  {
    XPoint p = XPoint((rect.width() >> 1) + rect.pos().x(), (rect.height() >> 1) + rect.pos().y());
    painter_set_color(mBrush);
    painter_fill_circle(p.x(), p.y(), rect.width() >> 1);
    painter_set_color(mPen);
    painter_draw_circle(p.x(), p.y(), rect.width() >> 1);
  }
  else
  {
    drawRect(rect);
  }
}

void XPainter::fillRect(const XRect& rect)
{
  painter_set_color(mBrush);
  painter_fill_rect(rect.pos().x(), rect.pos().y(), rect.size().w(), rect.size().h());
}

void XPainter::setWindow(const XRect& rect)
{
  painter_window(rect.pos().x(), rect.pos().y(), rect.size().w(), rect.size().h());
}

void XPainter::shear(const XPoint& pos)
{
  painter_offset_scr(pos.x(), pos.y());
}

void XPainter::setPen(uint32_t color)
{
  mPen = color;
}

void XPainter::setBrush(uint32_t color)
{
  mBrush = color;
}

void XPainter::reset()
{
  mPen = xRgba(0, 0, 0, 0);
  mBrush = xRgba(0, 0, 0, 0);
  painter_offset_scr(0, 0);
}

void XPainter::clear()
{
  painter_clear();
}

extern int32_t sy;
extern int32_t sx;

void XPainter::drawPixmap(const XPoint& pos, const XPixmap& pixmap)
{
  if(pixmap.bpp() == 4) painter_img32(pixmap.data(), pos.x() + sx, pos.y() + sy, pixmap.size().w(), pixmap.size().h());
  else if(pixmap.bpp() == 1)
  {
    painter_set_color(mPen);
    painter_img8(pixmap.data(), pos.x() + sx, pos.y() + sy, pixmap.size().w(), pixmap.size().h());
  }
}

void XPainter::drawLine(const XPoint& p1, const XPoint& p2)
{
  painter_set_color(mPen);
  painter_line(p1.x(), p1.y(), p2.x(), p2.y());
}

uint32_t XPainter::textWidth(const XString& text, const XFont *font)
{
  uint32_t len = text.lenght();
  if(font->isMonospace()) return len * font->size().w();
  const char *str = text.data();
  const uint8_t *wd = font->widthData();

  uint32_t w = 0;
  for(uint32_t i = 0; i < len; i++)
  {
    uint32_t s = *str++;
    if(s > 0x7a || s < 0x20) s = ' ';
    s -= 0x20;
    w += wd[s];
  }
  return w;
}

void XPainter::drawSymbol(XPoint& pos, uint32_t symbol, const XFont *font)
{
  const uint8_t *data = font->data();
  uint32_t w = font->size().w();
  uint32_t h = font->size().h();
  const uint8_t *wd = font->widthData();

  if(font->bpp() == 1)
  {
    for(uint32_t j = 0; j < h; j++)
      for(uint32_t i = 0; i < w; i++)
          if(data[symbol * w + i] & (1 << j)) painter_point(pos.x() + i + sx, pos.y() + j + sy);
  }
  else if(font->bpp() == 8)
  {
    painter_set_color(mPen);
    painter_img8(&data[w * h * symbol], pos.x() + sx, pos.y() + sy, w, h);
  }
  pos += XPoint(font->isMonospace() ? w + 1 : wd[symbol], 0);
}

void XPainter::drawText(const XPoint& pos, const XString& text, const XFont *font, AlignmentFlag flag)
{
  uint32_t len = text.lenght();
  uint32_t x = (flag & AlignHCenter) ? -(textWidth(text, font) >> 1) : 0;
  uint32_t y = (flag & AlignVCenter) ? -(font->size().h() >> 1) : 0;

  XPoint m_pos = pos + XPoint(x, y);

  const char *str = text.data();
  painter_set_color(mPen);

  for(uint32_t k = 0; k < len; k++)
  {
    uint32_t s = *str++;
    if(s > 0x7a || s < 0x20) s = ' ';
    s -= 0x20;

    drawSymbol(m_pos, s, font);
  }
}

void XPainter::drawText(const XPoint& pos, const XString& text, AlignmentFlag flag)
{
  drawText(pos, text, xSys()->font(), flag);
}







void painter_window(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  vbuf_minx = x;
  vbuf_miny = y;
  vbuf_maxx = vbuf_minx + w;
  vbuf_maxy = vbuf_miny + h;
}

void painter_offset_scr(int32_t x, int32_t y)
{
  sx = x;
  sy = y;
}

void painter_clear()
{
  painter_set_color(0xff000000);
  painter_fill_rect_dma_clut(0, 0, LCD_MAXX, LCD_MAXY);
}

#define swap(a, b) { int32_t t = a; a = b; b = t; }

void painter_line(int32_t x0, int32_t y0, int32_t x1, int32_t y1)
{
  int32_t dy = y1 - y0;
  int32_t dx = x1 - x0;
  int32_t steep = abs(dy) > abs(dx);

  if(steep)
  {
    swap(x0, y0);
    swap(x1, y1);
    swap(dx, dy);
  }

  dy = abs(dy);

  if(dx < 0)
  {
    swap(x0, x1);
    swap(y0, y1);
    dx = -dx;
  }

  int32_t err = dx >> 1;
  int32_t ystep;

  if(y0 < y1) ystep = 1;
  else ystep = -1;

  do
  {
    if(steep) painter_point(y0 + sx, x0 + sy);
    else painter_point(x0 + sx, y0 + sy);

    err -= dy;
    if(err < 0)
    {
      y0 += ystep;
      err += dx;
    }
  } while(x0++ < x1);
}

void painter_fill_triangle(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2)
{
  int32_t a, b, y, last;

  // sort coordinates by y order (y2 >= y1 >= y0)
  if(y0 > y1)
  {
    swap(y0, y1);
    swap(x0, x1);
  }
  if(y1 > y2)
  {
    swap(y2, y1);
    swap(x2, x1);
  }
  if(y0 > y1)
  {
    swap(y0, y1);
    swap(x0, x1);
  }

  if(y0 == y2)
  {
    a = b = x0;
    if(x1 < a) a = x1;
    else if(x1 > b) b = x1;
    if(x2 < a) a = x2;
    else if(x2 > b) b = x2;
    painter_draw_fast_hline(a, y0, b - a + 1);
    return;
  }

  int32_t dx01 = x1 - x0;
  int32_t dy01 = y1 - y0;
  int32_t dx02 = x2 - x0;
  int32_t dy02 = y2 - y0;
  int32_t dx12 = x2 - x1;
  int32_t dy12 = y2 - y1;
  int32_t sa = 0;
  int32_t sb = 0;

  if(y1 == y2) last = y1;
  else last = y1 - 1;

  for(y = y0; y <= last; y++)
  {
    a = x0 + sa / dy01;
    b = x0 + sb / dy02;
    sa += dx01;
    sb += dx02;
    if(a > b) swap(a, b);
    painter_draw_fast_hline(a + 1, y, b - a);
  }

  sa = dx12 * (y - y1);
  sb = dx02 * (y - y0);
  for(; y <= y2; y++)
  {
    a = x1 + sa / dy12;
    b = x0 + sb / dy02;
    sa += dx12;
    sb += dx02;
    if(a > b) swap(a, b);
    painter_draw_fast_hline(a + 1 , y, b - a);
  }
}

#define MIN(a, b) ((a < b) ? (a) : (b))
#define MAX(a, b) ((a > b) ? (a) : (b))

void painter_draw_fast_hline(int32_t x, int32_t y, int32_t w)
{
  int32_t x2 = x + w;
  x = MAX(x, vbuf_minx);
  x2 = MIN(x2, vbuf_maxx);
  w = x2 - x;
  if(y < vbuf_miny || y >= vbuf_maxy || w < 1) return;

  painter_draw_hline_clut(x, y, w);
}

void painter_draw_fast_vline(int32_t x, int32_t y, int32_t h)
{
  int32_t y2 = y + h;
  y = MAX(y, vbuf_miny);
  y2 = MIN(y2, vbuf_maxy);
  h = y2 - y;
  if(x < vbuf_minx || x >= vbuf_maxx || h < 1) return;

  painter_draw_vline_clut(x, y, h);
}

void painter_draw_rect(int32_t x, int32_t y, uint32_t w, uint32_t h)
{
  x += sx;
  y += sy;

  painter_draw_fast_hline(x, y, w);
  painter_draw_fast_vline(x + w, y, h);
  painter_draw_fast_hline(x + 1, y + h, w);
  painter_draw_fast_vline(x, y + 1, h);
}

void painter_fill_rect(int32_t x, int32_t y, int32_t w, int32_t h)
{
  x += sx;
  y += sy;
//  painter_fast_fill_rect_dma(vbuf_blending, x, y, w, h, c);
//  painter_copy_layer_rect(vbuf, vbuf_blending, x, y, w, h);

  int32_t y2 = y + h;
  y = MAX(y, vbuf_miny);
  y2 = MIN(y2, vbuf_maxy);
  h = y2 - y;

  int32_t x2 = x + w;
  x = MAX(x, vbuf_minx);
  x2 = MIN(x2, vbuf_maxx);
  w = x2 - x;

  if(h < 1 || w < 1) return;
  painter_fill_rect_dma_clut(x, y, w, h);
}

void painter_draw_circle(int32_t x, int32_t y, uint32_t r)
{
  int32_t f = 1 - r;
  int32_t ddF_x = 1;
  int32_t ddF_y = -2 * r;
  int32_t x1 = 0;
  int32_t y1 = r;

  painter_point(x + sx, y + sy + r);
  painter_point(x + sx, y + sy - r);
  painter_point(x + sx + r, y + sy);
  painter_point(x + sx - r, y + sy);

  while(x1 < y1)
  {
    if(f >= 0)
    {
      y1--;
      ddF_y += 2;
      f += ddF_y;
    }
    x1++;
    ddF_x += 2;
    f += ddF_x;

    painter_point(x + sx + x1, y + sy + y1);
    painter_point(x + sx - x1, y + sy + y1);
    painter_point(x + sx + x1, y + sy - y1);
    painter_point(x + sx - x1, y + sy - y1);

    painter_point(x + sx + y1, y + sy + x1);
    painter_point(x + sx - y1, y + sy + x1);
    painter_point(x + sx + y1, y + sy - x1);
    painter_point(x + sx - y1, y + sy - x1);
  }
}

void painter_fill_circle(int32_t x, int32_t y, uint32_t r)
{
  int32_t f = 1 - r;
  int32_t ddF_x = 1;
  int32_t ddF_y = -2 * r;
  int32_t x1 = 0;
  int32_t y1 = r;

  painter_draw_fast_vline(x, y + r, 1);
  painter_draw_fast_vline(x, y - r, 1);
  painter_draw_fast_vline(x + r, y, 1);
  painter_draw_fast_vline(x - r, y, 1);
  painter_draw_fast_vline(x - r, y, 2 * r);

  while(x1 < y1)
  {
    if(f >= 0)
    {
      y1--;
      ddF_y += 2;
      f += ddF_y;
    }
    x1++;
    ddF_x += 2;
    f += ddF_x;

    painter_draw_fast_vline(x - x1, y + y1, 2 * x1);
    painter_draw_fast_vline(x - x1, y - y1, 2 * x1);

    painter_draw_fast_vline(x - y1, y + x1, 2 * y1);
    painter_draw_fast_vline(x - y1, y - x1, 2 * y1);
  }
}
