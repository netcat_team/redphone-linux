/*
 * xwidget.cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#include "xwidget.h"

#include "xlist.h"
#include "xpoint.h"
#include "xsystem.h"

XWidget::XWidget(XObject *parent) : XObject(parent, true),
  mVisible(true), mEnabled(true), mGeometry(XRect()),
  mStyle(&xSys()->style()), mFont(xSys()->font())
{
  if(!parent) setParent(xSys()->topLevelWidget());
//  xSys()->syslog("created widget");
}

XWidget::~XWidget()
{
 // todo
  if(isWidget()) update();
}

bool XWidget::isActive()
{
  if(parent())
  {
    if(parent()->isWidget() && parent()->children()->first() == this)
      return reinterpret_cast<XWidget *>(parent())->isActive();
    else return false;
  }
  return true;
}

void XWidget::setActive()
{
  setWidgetOnTop();
  update();
}

XPoint XWidget::mapTo(XWidget *widget)
{
  XPoint pos;
  XObject *w = this;

  while(1)
  {
    if(w->parent() && w->parent()->isWidget() && w->parent() != widget)
    {
      pos += reinterpret_cast<XWidget*>(w->parent())->geometry().pos();
      w = w->parent();
    }
  }

  return pos;
}

XPoint XWidget::mapToGlobal()
{
  return mapTo(xSys()->topLevelWidget());
}

void XWidget::setGeometry(const XRect& geometry)
{
  mGeometry = geometry;
  update();
}

void XWidget::setWidgetOnTop()
{
  if(parent() && parent()->isWidget())
  {
    parentWidget()->children()->remove(this);
    parentWidget()->children()->add(this);
    parentWidget()->setWidgetOnTop();
  }
}

void XWidget::setFont(const XString &fontName)
{
  mFont = xSys()->font(fontName);
}

void XWidget::move(const XPoint& pos)
{
  mGeometry.move(pos);
  update();
}

void XWidget::resize(const XSize& size)
{
  mGeometry.resize(size);
  update();
}

void XWidget::update()
{
  xSys()->redraw();
}

