/*
 * xsystem.cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#include "xsystem.h"
#include "fonts/xfont.h"
extern "C" {
#include "sys/services/syslog.h"
}

XSystem* XSystem::mSelf = 0;

const XWidgetStyle mWidgetStyleStd = {
  xRgb(50, 120, 210), // borderColor
  xRgba(0, 0, 0, 190), // containerBgColor
  xRgba(100, 100, 150, 150), // containerFgColor
  xRgb(80, 80, 100), // componentBgColor
  xRgb(100, 100, 150), // componentBgTriggeredColor
  xRgb(220, 220, 255) }; // textColor

void XSystem::init()
{
  mFontList.load("/sys/fonts/chococooky.gff");

  mScreen = new XScreen(this);
  mTouch = new XTouch(mScreen);
  mGsmService = new XGsmService(this);
  mPainter = new XPainter(mScreen, mScreen->buf());
  mWallpaper = new XPixmap();
  mWallpaper->load("/sys/media/wallpaper.bmp");

  mTopLevelWidget = new XWidget(this);
  mTopLevelWidget->setGeometry(XRect(mScreen->size()));
  mTopLevelWidget->mArea = mTopLevelWidget->mGeometry;

  mSideBar = new XSideBar(mTopLevelWidget);
  mBattery = new XBattery(mSideBar);
  mModemIndicator = new XModemIndicator(mSideBar);
}

const XWidgetStyle &XSystem::style() const
{
  return mWidgetStyleStd;
}

void XSystem::redraw()
{
  if(!mGlLockCnt)
  {
//    syslog(SYSLOG_DEBUG, "call redraw");

    if(mTopLevelWidget)
    {
      mPainter->reset();
      // mPainter->clear();
      mPainter->drawPixmap(XPoint(0, 0), *mWallpaper);
      goTreeReverse(mTopLevelWidget, &XSystem::paintWidget);
      mScreen->push();
    }
  }
  else
  {
//    syslog(SYSLOG_DEBUG, "ignored redraw");
  }
}

bool XSystem::paintWidget(XObject *obj)
{
  XWidget *w = reinterpret_cast<XWidget *>(obj);

  if(w->isVisible())
  {
    if(w != mTopLevelWidget)
    {
      w->mPos = w->pos() + w->parentWidget()->mPos;
      w->mArea = XRect(w->mPos, w->size()) & w->parentWidget()->mArea;
    }
    mPainter->reset();
    mPainter->setWindow(w->mArea);
    mPainter->shear(w->mPos);
    w->paintEvent(mPainter);

//    mPainter->reset();
//    mPainter->setPen(xRgb(255, 255, 0));
//    mPainter->drawRect(w->mArea.adjusted(0, 0, -1, -1));

    return true;
  }
  return false;
}

void XSystem::goTreeReverse(XObject *w, bool (XSystem::*action)(XObject *obj))
{
  XObject *prev, *end = w;

  while(1)
  {
    if((this->*action)(w))
    {
      if(w->children()->isExist())
      {
        w = w->children()->last();
        continue;
      }
    }

    do
    {
      if(w == end) return;
      w = w->parent();
      prev = w->children()->prev();
    }
    while(!prev);
    w = prev;
  }
}

void XSystem::deliverTouch(XTouchEvent *e)
{
  if(e->status() == TS_STATUS_PRESSED)
  {
    goTreeForward(e, mTopLevelWidget, &XSystem::touchIf, &XSystem::touchWidget);
  }
  else
  {
    if(mLastFocus)
    {
      e->mPoint = e->mGlobal - mLastFocus->mPos;
      mLastFocus->tsEvent(e);
      if(e->status() == TS_STATUS_RELEASED)
      {
        mLastFocus = 0;
      }
    }
  }
}

bool XSystem::touchIf(XTouchEvent *e, XObject *obj)
{
  XWidget *w = reinterpret_cast<XWidget *>(obj);
  return w->isVisible() && w->mArea.inside(e->mGlobal);
}

bool XSystem::touchWidget(XTouchEvent *e, XObject *obj)
{
  XWidget *w = reinterpret_cast<XWidget *>(obj);

  w = reinterpret_cast<XWidget *>(obj);
  e->mPoint = e->mGlobal - w->mPos;
  w->tsEvent(e);

  if(e->isAccepted())
  {
    mLastFocus = w;
    mLastFocus->setActive();
    return true;
  }
  return false;
}

void XSystem::goTreeForward(XTouchEvent *e, XObject *w, bool (XSystem::*condition)(XTouchEvent *e, XObject *obj),
        bool (XSystem::*action)(XTouchEvent *e, XObject *obj))
{
  XObject *next, *end = w;
  int f = 0;

  while(1)
  {
    if((this->*condition)(e, w))
    {
      if(w->children()->isExist())
      {
        w = w->children()->first();
        continue;
      }
      else
      {
        if((this->*action)(e, w)) return;
      }
    }

    f = 0;
    do
    {
      if(f++)
      {
        if((this->*action)(e, w)) return;
      }
      if(w == end) return;
      w = w->parent();
      next = w->children()->next();
    }
    while(!next);
    w = next;
  }
}
