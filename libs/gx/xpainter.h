/*
 * xpainter.h
 *
 *  Created on: Jan 6, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XPAINTER_H_
#define GXLIB_XPAINTER_H_

#include "xpainter.h"
#include "xobject.h"
#include "xrect.h"
#include "xstring.h"
#include "xpixmap.h"
#include "fonts/xfont.h"

#define xRgba(r, g, b, a) ((uint32_t)(((a) << 24) | ((r) << 16) | ((g) << 8) | (b)))
#define xRgb(r, g, b) xRgba(r, g, b, 255)

typedef enum
{
  NoAlign = 0,

  AlignLeft = 0x01,
  AlignRight = 0x02,
  AlignHCenter = 0x04,

  AlignTop = 0x10,
  AlignBottom = 0x20,
  AlignVCenter = 0x04,

  AlignCenter = AlignHCenter | AlignVCenter
} AlignmentFlag;

class XPainter: public XObject
{
public:
  XPainter(XObject *obj = 0, uint8_t *buf = 0);
  virtual ~XPainter() {}

  void setBuf(uint8_t *buf) { mBuf = buf; }

  void drawRect(const XRect& rect);
  void fillRect(const XRect& rect);

  void drawEllips(const XRect& rect);

  void drawText(const XPoint& pos, const XString& text, const XFont *font, AlignmentFlag flag = NoAlign);
  void drawText(const XPoint& pos, const XString& text, AlignmentFlag flag = NoAlign);

  void drawLine(const XPoint& p1, const XPoint& p2);
  void drawPixmap(const XPoint& pos, const XPixmap& pixmap);

  void setWindow(const XRect& rect);
  void shear(const XPoint& pos);

  void setPen(uint32_t color);
  void setBrush(uint32_t color);

  void clear();
  void reset();

private:
  uint8_t *mBuf;
  uint32_t mPen;
  uint32_t mBrush;

  uint32_t textWidth(const XString& text, const XFont *font);
  void drawSymbol(XPoint& pos, uint32_t symbol, const XFont *font);
};

#endif /* GXLIB_XPAINTER_H_ */
