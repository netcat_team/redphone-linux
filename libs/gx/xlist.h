/*
 * xlist.h
 *
 *  Created on: Jan 7, 2016
 *      Author: netcat
 */

#ifndef GXLIB_XLIST_H_
#define GXLIB_XLIST_H_

template <class T> class XListNode
{
public:
  XListNode(T item, XListNode<T> *last = 0, XListNode<T> *next = 0) : mItem(item), mPrev(last), mNext(next) {}
  const XListNode<T> *next() const { return mNext; }
  const XListNode<T> *prev() const { return mPrev; }
  const T obj() const { return mItem; }

  // todo private
  T mItem;
  XListNode<T> *mPrev;
  XListNode<T> *mNext;
};

template <class T> class XList
{
public:
  XList() : mHead(0), mTail(0), mCurrent(0) {}

  void add(T item)
  {
    mCurrent = 0;

    if(mHead)
    {
      XListNode<T> *t = new XListNode<T>(item);
      t->mNext = mHead;
      mHead->mPrev = t;
      mHead = t;
    }
    else
    {
      mHead = new XListNode<T>(item);
      mHead->mPrev = 0;
      mHead->mNext = 0;
      mTail = mHead;
    }
  }

  void push_back(T item)
  {
    mCurrent = 0;

    if(mTail)
    {
      XListNode<T> *t = new XListNode<T>(item);
      mTail->mNext = t;
      t->mPrev = mTail;
      t->mNext = 0;
      mTail = t;
    }
    else
    {
      mTail = new XListNode<T>(item);
      mTail->mPrev = 0;
      mTail->mNext = 0;
      mHead = mTail;
    }
  }

  bool remove(T item)
  {
    mCurrent = 0;

    XListNode<T> *p = mHead;

    while(p)
    {
      if(p->mItem == item)
      {
        if(p->mPrev) p->mPrev->mNext = p->mNext;
        else mHead = p->mNext;

        if(p->mNext) p->mNext->mPrev = p->mPrev;
        else mTail = p->mPrev;

        delete p;
        return true;
      }
      p = p->mNext;
    }
    return false;
  }

  void clear()
  {
    mCurrent = 0;

    XListNode<T> *t, *p = mHead;

    while(p)
    {
      t = p->mNext;
      delete p;
      p = t;
    }
  }

  bool isExist() const { return mHead ? true : false; }

  XListNode<T> *first() const { return mHead; }
  T first()
  {
    mCurrent = mHead;
    return mCurrent ? mCurrent->mItem : 0;
  }

  XListNode<T> *last() const { return mTail; }
  T last()
  {
    mCurrent = mTail;
    return mCurrent ? mCurrent->mItem : 0;
  }

  T next()
  {
    if(mCurrent) mCurrent = mCurrent->mNext;
    return mCurrent ? mCurrent->mItem : 0;
  }
  T prev()
  {
    if(mCurrent) mCurrent = mCurrent->mPrev;
    return mCurrent ? mCurrent->mItem : 0;
  }

  ~XList()
  {
    XListNode<T> *p = mHead, *next;
    while(p)
    {
      next = p->mNext;
      delete p;
      p = next;
    }
  }

private:
  XListNode<T> *mHead, *mTail;
  XListNode<T> *mCurrent;
};

#endif /* GXLIB_XLIST_H_ */
