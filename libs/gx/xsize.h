#ifndef XSIZE_H
#define XSIZE_H

#include "xobject.h"

#define xMax(a, b) ((a) > (b) ? (a) : (b))
#define xMin(a, b) ((a) > (b) ? (b) : (a))

template <class T>
class TSize
{
    public:
        TSize(T w = 0, T h = 0) : mW(w), mH(h) { if(mW < 0) mW = 0; if(mH < 0) mH = 0; }
        TSize(T size) : mW(size), mH(size) { if(mW < 0) mW = 0; if(mH < 0) mH = 0; }

        // get
        T w() const { return mW; }
        T h() const { return mH; }
        T& rw() { return mW; }
        T& rh() { return mH; }

        void resize(T w, T h) { mW = xMax(0, w); mH = xMax(0, h); }

        // set
        void setW(T w) { mW = xMax(0, w); }
        void setH(T h) { mH = xMax(0, h); }

        TSize<T>& boundedTo(const TSize<T>& size)
        {
            mW = xMin(mW, size.mW);
            mH = xMin(mH, size.mH);
            return *this;
        }

        TSize<T>& expandedTo(const TSize<T>& size)
        {
            mW = xMax(mW, size.mW);
            mH = xMax(mH, size.mH);
            return *this;
        }

        TSize<T>& half() { return XSize(mW >> 1, mH >> 1); }

        TSize<T> operator+(const TSize<T> &size) { return TSize<T>(mW + size.w(), mH + size.h()); }
        TSize<T> operator-(const TSize<T> &size) { return TSize<T>(mW - size.w(), mH - size.h()); }
        void operator+=(const TSize<T> &size) { mW += size.w(); mH += size.h(); }
        void operator-=(const TSize<T> &size) { mW -= size.w(); mH -= size.h(); }

        bool operator==(const TSize<T>& size) const { return mH == size.w() && mW == size.w(); }
        bool operator!=(const TSize<T>& size) const { return mH != size.w() || mW != size.w(); }

        bool isExist() const { return ((mW > 0) && (mH > 0)); }

        void transpose() { xSwap(mW, mH); }

        T square() const { return mW * mH; }

    private:
        T mW, mH;
};

typedef TSize<int32_t> XSize;

#endif // XSIZE_H
