/*
 * xscreen.cpp
 *
 *  Created on: Feb 1, 2016
 *      Author: netcat
 */

#include "xscreen.h"
#include "xsystem.h"

extern "C" {
#include "gma.h"
}

XScreen::XScreen(XObject *obj = 0) : XObject(obj),
  mOr(SCR_PORTAIN), mSize(XSize(LCD_MAXX, LCD_MAXY)), mBuf(0)
{

}

void XScreen::push()
{
  lcd_update();
}
