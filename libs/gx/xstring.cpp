/*
 * xstring.cpp
 *
 *  Created on: Jan 19, 2016
 *      Author: netcat
 */

#include "xstring.h"

extern "C" {
#include "sys/sys.h"
}

#define xMin(a, b) ((a) > (b) ? (b) : (a))

XString::XString()
    : mSize(0), mData(0)
{
}

XString::XString(const char *str)
{
  mSize = strlen(str);
  mData = new char[mSize + 1];
  memcpy(mData, str, mSize);
  mData[mSize] = 0;
}

XString::XString(const XString& str)
  : mSize(str.mSize)
{
  mData = new char[mSize + 1];
  memcpy(mData, str.mData, mSize);
  mData[mSize] = 0;
}

XString::XString(const char *str, uint32_t size)
  : mSize(size)
{
  mData = new char[mSize + 1];
  memcpy(mData, str, size);
  mData[mSize] = 0;
}

XString::XString(char c)
  : mSize(1)
{
  mData = new char[mSize + 1];
  mData[0] = c;
  mData[1] = 0;
}

XString::~XString()
{
  delete[] mData;
}

XString& XString::operator=(const XString &str)
{
  delete[] mData;

  mSize = str.mSize;
  mData = new char[mSize + 1];
  memcpy(mData, str.mData, mSize);
  mData[mSize] = 0;

  return *this;
}

XString& XString::operator+=(const XString &str)
{
  uint32_t t_size = mSize + str.mSize;
  char *t = new char[t_size + 1];
  memcpy(t, mData, mSize);
  memcpy(t + mSize, str.mData, str.mSize);
  t[t_size] = 0;

  delete[] mData;
  mData = t;
  mSize = t_size;

  return *this;
}

bool XString::operator==(const XString &str) const
{
  if(mSize && str.mSize)
    return strcmp(mData, str.mData) ? false : true;
  else return false;
}

void XString::chop(uint32_t n)
{
  if(n >= mSize)
  {
    mSize = 0;
    delete[] mData;
  }
  else
  {
    mSize -= n;
    mData[mSize] = 0;
  }
}

void XString::truncate(uint32_t pos)
{
  mSize = xMin(mSize, pos);
  if(!mSize) delete[] mData;
  else mData[mSize] = 0;
}

XString XString::left(uint32_t n) const
{
  return XString(mData, xMin(mSize, n));
}

XString XString::right(uint32_t n) const
{
  if(n < mSize) return XString(mData + (mSize - n), n);
  return XString();
}

void XString::fill(char c, uint32_t size)
{
  if(mSize < size)
  {
    delete[] mData;
    mSize = size;
    mData = new char[mSize + 1];
  }

  memset(mData, c, size);
  mData[size] = 0;
}

void itoa_h32(uint32_t n, char *s)
{
  uint32_t cnt;
  if(n)
  {
    uint32_t t = n;
    cnt = 0;
    while(n)
    {
      n >>= 4;
      cnt += 4;
    }
    n = t;
  }
  else cnt = 4;

  while(cnt)
  {
    cnt -= 4;
    char c = (n >> cnt) & 0xf;
    if(c >= 0x0a) c += 'a' - 0x0a;
    else c += '0';
    *s++ = c;
  }
  *s++ = 0;
}

XString XString::number(int32_t n, uint32_t base)
{
  if(base == 16)
  {
    char str[9];
    itoa_h32(n, str);
    return XString(str);
  }
  return XString();
}

