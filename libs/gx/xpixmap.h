/*
 * xpixmap.h
 *
 *  Created on: Nov 1, 2017
 *      Author: netcat
 */

#ifndef GXLIB_XPIXMAP_H_
#define GXLIB_XPIXMAP_H_

#include "xsize.h"
#include "xstring.h"

extern "C" {
#include "libs/fatfs/fatio.h"
}

class XPixmap
{
public:
  XPixmap();
  virtual ~XPixmap();

  const uint8_t *data() const { return mData; }
  const XSize &size() const { return mSize; }
  uint32_t bytes() const { return mSize.w() * mSize.h(); }
  uint32_t bpp() const { return mBpp; }

  void load(const XString &fileName);
  void loadFromData(const uint8_t *data, const XSize &size);
  bool isNull() const { return mData; };

private:
  uint8_t *mData;
  XSize mSize;
  uint32_t mBpp;

};

#endif /* GXLIB_XPIXMAP_H_ */
