#ifndef FAT32_H
#define FAT32_H

#include <stdint.h>
#include "mbr.h"

#define FAT_CLUSTER_SIZE 512
#define FAT_ROOT_CLUSTER 2
#define FAT_EOC1 0x0FFFFFF8
#define FAT_EOC2 0x0FFFFFFF

enum
{
  FILE_ATTR_READ_ONLY = 0x01,
  FILE_ATTR_HIDEN = 0x02,
  FILE_ATTR_SYSTEM = 0x04,
  FILE_ATTR_VOLUME_ID = 0x08,
  FILE_ATTR_DIR = 0x10,
  FILE_ATTR_ARCHIVE = 0x20,
  FILE_ATTR_LONG_NAME = 0x0f,
  FILE_ATTR_NONDELETE = 0xc0
};

enum
{
  FAT_FILE_EMPTY = 0x00,
  FAT_FILE_PENDING_ERASE = 0x05,
  FAT_FILE_ERASED = 0xe5,
  FAT_FILE_DOT_ENTRY = 0x2e,
};

// 36 bytes
typedef struct __attribute__ ((packed))
{
  uint8_t jmp_boot[3];
  uint8_t oem_name[8];
  uint16_t bytes_in_sector;
  uint8_t sector_in_cluster;
  uint16_t reserved_sectors;
  uint8_t num_fats;
  uint16_t root_count;
  uint16_t total_sectors16;
  uint8_t media;
  uint16_t fat16_sectors;
  uint16_t sector_in_track;
  uint16_t heads_num;
  uint32_t hidden_sectors;
  uint32_t total_sectors32;
} fat_bpb_t;

// 54 bytes
typedef struct __attribute__ ((packed))
{
  uint32_t fat32_sectors;
  uint16_t ext_flags;
  uint16_t fs_ver;
  uint32_t root_cluster;
  uint16_t fs_info;
  uint16_t bk_boot_sec;
  uint8_t res0[12];
  uint8_t drive_num;
  uint8_t res1;
  uint8_t boot_sig;
  uint32_t volume_id;
  uint8_t volume_label[11];
  uint8_t fs_type[8];
} fat_bpb32_t;

// 32 bytes
typedef struct __attribute__ ((packed))
{
  uint8_t name[11];
  uint8_t attributes;
  uint8_t res;
  uint8_t create_time_10ms;
  uint16_t create_time;
  uint16_t create_date;
  uint16_t last_access_date;
  uint16_t first_cluster_hi;
  uint16_t write_time;
  uint16_t write_date;
  uint16_t first_cluster_lo;
  uint32_t file_size;
} fat_dir_t;

typedef struct __attribute__ ((packed))
{
  uint8_t id;
  uint8_t name0_4[10];
  uint8_t attr;
  uint8_t res0;
  uint8_t alias_checksum;
  uint8_t name5_10[12];
  uint8_t res1[2];
  uint8_t name11_12[4];
} fat_long_name_t;

uint32_t fat_init(uint8_t *buf);
void fat_cluster(uint8_t *buf, uint32_t cluster);
uint32_t fat_table(uint8_t *buf, uint32_t cluster);
fat_dir_t *fat_find_path(uint8_t *buf, const char *path);


#endif
