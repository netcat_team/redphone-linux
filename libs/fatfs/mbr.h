#ifndef MBR_H
#define MBR_H

#include <stdint.h>

#define MBR_SIGNATURE 0xaa55

typedef enum
{
  MBR_LS_TYPE_FAT32 = 0x0b,
  MBR_LS_TYPE_LINUX = 0x83,
} mbr_fs_type_t;

// partition entry
typedef struct __attribute__ ((packed))
{
 uint8_t status;
 uint8_t chs[3];
 uint8_t type;
 uint8_t chs_last[3];
 uint32_t start;
 uint32_t count;
} fs_mbr_pe_t;

// generic MBR
typedef struct __attribute__ ((packed))
{
  uint8_t bootloader[446];
  fs_mbr_pe_t partition[4];
  uint16_t signature;
} fs_mbr_t;

uint32_t mbr_find_part(uint8_t *buf, mbr_fs_type_t type);

#endif
