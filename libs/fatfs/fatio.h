/*
 * fatio.h
 *
 *  Created on: Oct 24, 2017
 *      Author: netcat
 */

#ifndef FAT_IO_H
#define FAT_IO_H

typedef struct
{
  uint32_t start_cluster_addr;
  uint32_t cur_cluster_addr;
  uint32_t pos;
  uint32_t size;
  uint8_t *buf;
} fat_file_t;

fat_file_t *f_open(const char *path);
void f_seek(fat_file_t *file, uint32_t len);
uint32_t f_read(fat_file_t *file, uint8_t *buf, uint32_t size);
void f_close(fat_file_t *file);

#endif
