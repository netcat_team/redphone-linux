/*
 * fatfs.c
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */

#include "fatfs.h"
#include "sys.h"

uint32_t fat_fs_addr, fat_data_addr;
uint32_t fat_loaded_cluster;

uint8_t fat_name_buf[48];
uint32_t fat_name_len;

void fat_cluster(uint8_t *buf, uint32_t cluster)
{
  if(cluster != fat_loaded_cluster)
  {
    sdhc_read_block(buf, fat_data_addr + cluster - 2);
    fat_loaded_cluster = cluster;
  }
}

uint32_t fat_table(uint8_t *buf, uint32_t cluster)
{
  uint32_t b_offset, s_offset;

  // printf("->");
  cluster &= 0x0fffffff;
  s_offset = (cluster >> 7) + fat_fs_addr;
  b_offset = (cluster & 0x7f) << 2;

  fat_loaded_cluster = cluster;
  sdhc_read_block(buf, s_offset);

  uint8_t *ptr = &buf[b_offset];
  return *((uint32_t*)ptr);
}

static void fat_unicode_append(uint8_t *buf, uint32_t cnt)
{
  uint32_t i;
  uint32_t shift = 0;

  for(i = 0; i < cnt * 2; i += 2)
    if(buf[i] != 0 && buf[i] != 0xff) shift++;

  fat_name_len += shift;
  if(fat_name_len > sizeof(fat_name_buf) - 1) fat_name_len = sizeof(fat_name_buf) - 1;

  while(shift--)
    for(i = fat_name_len + 1; i > 0; i--)
      fat_name_buf[i] = fat_name_buf[i - 1];

  uint8_t *t = fat_name_buf;
  while(cnt--)
  {
    if(*buf == 0 || *buf == 0xff) break;
    *t++ = *buf;
    buf += 2;
  }
  fat_name_buf[fat_name_len] = 0;
}

void fat_long_name(void *buf)
{
  fat_unicode_append(((fat_long_name_t *)buf)->name11_12, 2);
  fat_unicode_append(((fat_long_name_t *)buf)->name5_10, 6);
  fat_unicode_append(((fat_long_name_t *)buf)->name0_4, 5);
}

// void print_name(uint8_t *buf, uint32_t size)
// {
//   uint32_t i;
//   for(i = 0; i < size; i++) printf("%c", buf[i]);
// }

static void fat_unicode_reset()
{
  fat_name_buf[0] = 0;
  fat_name_len = 0;
}

fat_dir_t *fat_find_file(uint8_t *buf, const char *find_name, uint32_t name_size, uint32_t addr)
{
  uint32_t i = 0;
  fat_dir_t *p = (fat_dir_t *)buf;

  fat_cluster(buf, addr);
  fat_unicode_reset();

  while(1)
  {
    if(p[i].name[0] != FAT_FILE_EMPTY &&
       p[i].name[0] != FAT_FILE_ERASED &&
       p[i].attributes != FILE_ATTR_VOLUME_ID &&
       p[i].attributes != FILE_ATTR_LONG_NAME)
    {
      uint32_t f = strncmp(find_name, (const char*)fat_name_buf, name_size); // memcmp
//      printf("%s", name);
      fat_unicode_reset();
//      printf(" | ");
//      print_name(p[i].name, 11);
//      printf("\n");

      if(!f) return &p[i];
    }
    else if(p[i].attributes == FILE_ATTR_LONG_NAME)
    {
//      printf("* long\n");
      fat_long_name(&p[i]);
    }
    else if(p[i].name[0] == FAT_FILE_ERASED ||
            p[i].name[0] == FAT_FILE_PENDING_ERASE)
    {
//      printf("%s", name);
      fat_unicode_reset();
//      printf("* erased\n");
    }
    else if(p[i].attributes == FILE_ATTR_VOLUME_ID)
    {
//      print_name(p[i].name, 11);
//      printf("* volume id\n");
    }
    else if(p[i].name[0] == FAT_FILE_EMPTY) {}
    else syslog(SYSLOG_ERROR, "unknown record");

    i++;
    if(i >= 16)
    {
      uint32_t next = fat_table(buf, addr);
      if(next != FAT_EOC1 && next != FAT_EOC2)
      {
        i = 0;
        addr = next;
        fat_cluster(buf, addr);
      }
      else break;
    }
  }
  return 0;
}

fat_dir_t *fat_find_path(uint8_t *buf, const char *path)
{
  uint32_t i = 0, st = 0;
  fat_dir_t *dir = 0;
  uint32_t addr = FAT_ROOT_CLUSTER;

  while(1)
  {
    uint8_t s = path[i];

    if(s == '/' && i == 0) st = 1;
    else if(s == '/')
    {
      dir = fat_find_file(buf, &path[st], i - st, addr);
      if(!dir) break;
      addr = (dir->first_cluster_lo | (dir->first_cluster_hi << 16));
      st = i + 1;
    }
    else if(!s)
    {
      dir = fat_find_file(buf, &path[st], i - st, addr);
      if(!dir) break;
      addr = (dir->first_cluster_lo | (dir->first_cluster_hi << 16));
      break;
    }
    i++;
  }
  return dir;
}

uint32_t fat_init(uint8_t *buf)
{
  fat_loaded_cluster = 0;

  fat_fs_addr = mbr_find_part(buf, MBR_LS_TYPE_FAT32);
  if(fat_fs_addr)
  {
    syslog(SYSLOG_DEBUG, "FAT32 found in MBR");
    sdhc_read_block(buf, fat_fs_addr);
    fat_bpb_t *fat_bpb = (fat_bpb_t*)buf;
    fat_bpb32_t *fat_bpb32 = (fat_bpb32_t*)(buf + sizeof(fat_bpb_t));
    fat_fs_addr += fat_bpb->reserved_sectors;

    if(fat_bpb->sector_in_cluster != 1)
    {
      syslog(SYSLOG_ERROR, "sector_in_cluster: %d\n", fat_bpb->sector_in_cluster);
      fat_fs_addr = 0;
      return 0;
    }

    fat_data_addr = fat_fs_addr + fat_bpb->num_fats * fat_bpb32->fat32_sectors;
  }
  else
  {
    fat_fs_addr = 0;
    fat_data_addr = 0;

    syslog(SYSLOG_ERROR, "FAT32 not found in MBR!");
  }
  return fat_fs_addr;
}
