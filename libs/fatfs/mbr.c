/*
 * mbr.c
 *
 *  Created on: Apr 8, 2017
 *      Author: netcat
 */
#include "mbr.h"

uint32_t mbr_find_part(uint8_t *buf, mbr_fs_type_t type)
{
  sdhc_read_block(buf, 0);
  fs_mbr_t *mbr = (fs_mbr_t*)buf;

  if(mbr->signature != MBR_SIGNATURE) return 0;

  uint32_t i;
  for(i = 0; i < 4; i++)
    if(mbr->partition[i].type == type)
      return mbr->partition[i].start;

  return 0;
}
