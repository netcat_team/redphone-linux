/*
 * fatio.h
 *
 *  Created on: Oct 24, 2017
 *      Author: netcat
 */

#include "fatfs.h"
#include "fatio.h"
#include <sys.h>
#include <stdlib.h>

fat_file_t *f_open(const char *path)
{
  fat_file_t *file = pvPortMalloc(sizeof(fat_file_t));
  file->buf = pvPortMalloc(FAT_CLUSTER_SIZE);

  fat_dir_t *dir = fat_find_path(file->buf, path);
  if(dir)
  {
    file->start_cluster_addr = (dir->first_cluster_lo | (dir->first_cluster_hi << 16));
    file->cur_cluster_addr = file->start_cluster_addr;
    file->pos = 0;
    file->size = dir->file_size;

//    syslog(SYSLOG_DEBUG, "f_open: %s", path);
    fat_cluster(file->buf, file->start_cluster_addr);

    return file;
  }
  else
  {
    vPortFree(file->buf);
    vPortFree(file);
  }

  return 0;
}

void f_seek(fat_file_t *file, uint32_t len)
{
  if(file)
  {
    if(len < file->size)
    {
      uint32_t addr = file->start_cluster_addr;
      file->pos = len;
      while(len > FAT_CLUSTER_SIZE)
      {
        addr = fat_table(file->buf, addr);
        len -= FAT_CLUSTER_SIZE;
      }
      file->cur_cluster_addr = addr;
      fat_cluster(file->buf, file->cur_cluster_addr);
    }
    else
    {
      syslog(SYSLOG_ERROR, "bad seek!");
    }
  }
}

void f_close(fat_file_t *file)
{
  if(file)
  {
    vPortFree(file->buf);
    vPortFree(file);
  }

//  syslog(SYSLOG_DEBUG, "f_close");
}

uint32_t f_read(fat_file_t *file, uint8_t *buf, uint32_t size)
{
  uint32_t left = file->size - file->pos;
  uint32_t out_cnt = 0;

  if(file)
  {
    if(size > left) size = left;

    while(size)
    {
      fat_cluster(file->buf, file->cur_cluster_addr);

      int pos_in_cluster = file->pos & (FAT_CLUSTER_SIZE - 1);
      int left_in_cluster = FAT_CLUSTER_SIZE - pos_in_cluster;

      if(size < left_in_cluster)
      {
        memcpy(&buf[out_cnt], file->buf + pos_in_cluster, size);
        out_cnt += size;
        file->pos += size;
        break;
      }
      else
      {
        memcpy(&buf[out_cnt], file->buf + pos_in_cluster, left_in_cluster);
        file->cur_cluster_addr = fat_table(file->buf, file->cur_cluster_addr);

        out_cnt += left_in_cluster;
        size -= left_in_cluster;
        file->pos += left_in_cluster;
      }
    }
  }
  return out_cnt;
}
