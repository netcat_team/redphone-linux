NAME = test
ODIR = obj
MCU = cortex-m4

# tool
ifeq (${PLATFORM},stm32)
  TOOLCHAIN = arm-none-eabi-
else
  TOOLCHAIN = 
endif

CC = $(TOOLCHAIN)gcc
CPPC = $(TOOLCHAIN)g++
OBJCOPY = $(TOOLCHAIN)objcopy
GDB = $(TOOLCHAIN)gdb
SIZE = $(TOOLCHAIN)size

# ld
ifeq (${PLATFORM},stm32)
  LDFILE = bsp/stm32/link.ld
else
  LDFILE = bsp/linux/link.ld
endif

# freertos
SRC += libs/freertos/croutine.c
SRC += libs/freertos/event_groups.c
SRC += libs/freertos/list.c
SRC += libs/freertos/queue.c
SRC += libs/freertos/tasks.c
SRC += libs/freertos/timers.c
SRC += libs/freertos/portable/heap_4.c

ifeq (${PLATFORM},stm32)
  SRC += libs/freertos/portable/stm32/port.c
else
  SRC += libs/freertos/portable/posix/port.c
endif

# fatfs
SRC += libs/fatfs/mbr.c
SRC += libs/fatfs/fatfs.c
SRC += libs/fatfs/fatio.c
# gx
CPPSRC += libs/gx/xobject.cpp
CPPSRC += libs/gx/xpainter.cpp
CPPSRC += libs/gx/xsystem.cpp
CPPSRC += libs/gx/xwidget.cpp
CPPSRC += libs/gx/xstring.cpp
CPPSRC += libs/gx/xscreen.cpp
CPPSRC += libs/gx/xtouch.cpp
CPPSRC += libs/gx/xgsmservice.cpp
CPPSRC += libs/gx/xpixmap.cpp
CPPSRC += libs/gx/widgets/xlabel.cpp
CPPSRC += libs/gx/widgets/xwindow.cpp
CPPSRC += libs/gx/widgets/xpushbutton.cpp
CPPSRC += libs/gx/widgets/xtetrix.cpp
CPPSRC += libs/gx/widgets/xswitch.cpp
CPPSRC += libs/gx/widgets/xslider.cpp
CPPSRC += libs/gx/widgets/xsidebar.cpp
CPPSRC += libs/gx/widgets/notify/xbattery.cpp
CPPSRC += libs/gx/widgets/notify/xmodemindicator.cpp
CPPSRC += libs/gx/fonts/xfont.cpp
CPPSRC += libs/gx/fonts/xfontlist.cpp
# microrl
SRC += libs/microrl/microrl.c
# printf
SRC += libs/printf/printf.c
# getopt
SRC += libs/getopt/getopt.c

# bsp
ifeq (${PLATFORM},stm32)
  SRC += bsp/stm32/cmsis/startup_stm32f4xx.c
  SRC += bsp/stm32/cmsis/system_stm32f4xx.c

  SRC += bsp/stm32/spl/stm32f4xx_dma.c
  SRC += bsp/stm32/spl/stm32f4xx_dma2d.c
  SRC += bsp/stm32/spl/stm32f4xx_fmc.c
  SRC += bsp/stm32/spl/stm32f4xx_gpio.c
  SRC += bsp/stm32/spl/stm32f4xx_i2c.c
  SRC += bsp/stm32/spl/stm32f4xx_ltdc.c
  SRC += bsp/stm32/spl/stm32f4xx_rcc.c
  SRC += bsp/stm32/spl/stm32f4xx_spi.c
  SRC += bsp/stm32/spl/stm32f4xx_usart.c
  SRC += bsp/stm32/spl/stm32f4xx_sdio.c
  SRC += bsp/stm32/spl/misc.c

  SRC += bsp/stm32/ub_lib/stm32_ub_i2c3.c

  SRC += bsp/stm32/rm68120.c
  SRC += bsp/stm32/bsp.c
  SRC += bsp/stm32/gma.c
  SRC += bsp/stm32/ts.c

  SRC += bsp/stm32/hal/hw_dma2d.c
  SRC += bsp/stm32/hal/hw_fmc.c
  SRC += bsp/stm32/hal/hw_gpio.c
  SRC += bsp/stm32/hal/hw_i2c.c
  SRC += bsp/stm32/hal/hw_ltdc.c
  SRC += bsp/stm32/hal/hw_rcc.c
  SRC += bsp/stm32/hal/hw_sdio.c
  SRC += bsp/stm32/hal/hw_spi.c
  SRC += bsp/stm32/hal/hw_uart.c
else
  SRC += bsp/linux/gma.c
  SRC += bsp/linux/ts.c
  SRC += bsp/linux/sd.c
  SRC += bsp/linux/freertos_dep.c
  SRC += bsp/linux/tty_uart.c
  SRC += bsp/linux/bsp.c
  CPPSRC += bsp/linux/alloc.cpp
endif

# sys
SRC += sys/drivers/gsm.c
SRC += sys/drivers/gsm_fsm.c
SRC += sys/services/ipc.c
SRC += sys/services/syslog.c
SRC += sys/apps.c
SRC += sys/cli.c

OBJS = $(patsubst %,$(ODIR)/%, ${SRC:.c=.o})

#cpp src
CPPSRC += main.cpp

# apps
CPPSRC += $(wildcard apps/*.cpp) $(wildcard apps/*/*.cpp)

CPPOBJS = $(patsubst %,$(ODIR)/%, ${CPPSRC:.cpp=.o})

#inc
INC += .
INC += apps
INC += sys
INC += bsp

INC += libs
INC += libs/microrl
INC += libs/fatfs
INC += libs/freertos
INC += libs/freertos/include
INC += libs/freertos/portable

ifeq (${PLATFORM},stm32)
  INC += bsp/stm32
  INC += bsp/stm32/cmsis
  INC += bsp/stm32/spl
  INC += bsp/stm32/ub_lib
  INC += libs/freertos/portable/stm32
else
  INC += bsp/linux
  INC += libs/freertos/portable/posix
endif

#cpp inc
INC += libs/gx
INC += libs/gx/widgets

_INC = $(INC:%=-I%)


CFLAGS = -g -Wall -O0 -ffunction-sections -fdata-sections
# CFLAGS += -Wno-implicit-function-declaration

ifeq (${PLATFORM},stm32)
  CFLAGS += -mcpu=$(MCU) -mthumb -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -fno-builtin
  CFLAGS += -DSTM32F429_439xx -DUSE_STDPERIPH_DRIVER
else
  CFLAGS += -lSDL -I/usr/include/SDL
endif

CPPFLAGS = $(CFLAGS) -std=c++11 -fno-rtti -fno-exceptions

LDFLAGS = -T${LDFILE} -Wl,--gc-sections -flto

ifeq (${PLATFORM},stm32)
  LDFLAGS += -mcpu=$(MCU) -mthumb --specs=nosys.specs -nostartfiles
else
  LDFLAGS += -lSDL -pthread 
endif


.PHONY : all
all: $(NAME).elf

$(ODIR)/%.o: %.c
	@mkdir -p $(ODIR)/$(dir $<)
	@echo "  CC\t$<"
	@$(CC) $(CFLAGS) $(_INC) -c $< -o $@

$(ODIR)/%.o: %.cpp
	@mkdir -p $(ODIR)/$(dir $<)
	@echo "  CC\t$<"
	@$(CPPC) $(CPPFLAGS) $(_INC) -c $< -o $@

$(NAME).elf: $(OBJS) $(CPPOBJS)
	@echo "  LD\t$@"
	@$(CPPC) $^ -o $@ $(LDFLAGS)
	@$(SIZE) $(NAME).elf

clean:
	@rm -f $(OBJS)
	@rm -f $(CPPOBJS)
	@rm -rf $(ODIR)
	@rm -f $(NAME).elf $(NAME).bin $(NAME).map

