#include "xsystem.h"

static uint8_t buf[FAT_CLUSTER_SIZE];

void sys_task(void *pvParameters)
{
  fat_init(buf);
  apps_init();

  while(1)
  {
    xSys()->ts()->update();
    xSys()->modem()->pollIpc();

    vTaskDelay(1);
  }
}

int main()
{
  bsp_init();
  cli_init();

  xTaskCreate(sys_task, "SYS", 2048, 0, configMAX_PRIORITIES, 0);
  vTaskStartScheduler();

  return 0;
}


