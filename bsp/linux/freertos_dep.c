/*
 * freertos_dep.c
 *
 *  Created on: Nov 2, 2018
 *      Author: netcat
 */
#include "sys.h"
#include "FreeRTOS.h"
#include "task.h"

void vApplicationIdleHook( void )
{
    //vFullDemoIdleFunction();
  void UART_IRQHandler(void);
  UART_IRQHandler();
  USART1_IRQHandler();
  // cli_poll();
}

void vApplicationTickHook(void)
{
  // vQueueOverwritePeriodicISRDemo();
}

void vAssertCalled(unsigned long ulLine, const char * const pcFileName)
{
  taskENTER_CRITICAL();
  {
    syslog(SYSLOG_ERROR, "[ASSERT] %s:%lu", pcFileName, ulLine);
    fflush(stdout);
  }
  taskEXIT_CRITICAL();
}

void vApplicationMallocFailedHook( void )
{
  vAssertCalled( __LINE__, __FILE__ );
}
