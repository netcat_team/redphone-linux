#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "tty_uart.h"
#include "sys/sys.h"

int F_ID = -1;

int tty_in(char *buff, int size)
{
  int n = read(F_ID, buff, size);

  if(n == -1)
  {
//    char *errmsg = strerror(errno);
//    printf("%s!\n", errmsg);
    return 0;
  }
  return n;
}

void bsp_linux_term_cfg(int silent_mode)
{
  struct termios t;
  tcgetattr(STDIN_FILENO, &t);
  if(silent_mode) t.c_lflag &= ~( ICANON | ECHO );
  else t.c_lflag |= ICANON | ECHO;
  tcsetattr( STDIN_FILENO, TCSANOW, &t);
}

int modem_serial_out(const char *buff, int len)
{
    int n = write(F_ID, buff, len);
//    if(n == -1)
//    {
//        char *errmsg = strerror(errno);
//        syslog(SYSLOG_ERROR, "%s", errmsg);
//    }
    return n;
}

int modem_serial_init()
{
  const char *tty_str = "/dev/ttyVUSB0";
  return tty_open(tty_str, 115200);
}

int tty_open(const char *tty_str, speed_t speed)
{
    F_ID = open(tty_str, O_RDWR | O_NOCTTY);
    if(F_ID == -1)
    {
      char *errmsg = strerror(errno);
      syslog(SYSLOG_ERROR, "%s: %s", tty_str, errmsg);
      return 1;
    }
    else
    {
      syslog(SYSLOG_DEBUG, "%s: opened", tty_str);
    }
    struct termios options;
    tcgetattr(F_ID, &options);

    cfsetispeed(&options, speed);
    cfsetospeed(&options, speed);

    options.c_cc[VTIME] = 1; /*Время ожидания байта 20*0.1 = 2 секунды */
    options.c_cc[VMIN] = 0;
 
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;
    
    options.c_lflag = 0;
    options.c_oflag &= ~OPOST;

    tcsetattr(F_ID, TCSANOW, &options);
    
    // char t;
    // int n = 1;
    // while(n) n = read(F_ID, &t, 1);

    return 0;
}

void tty_close()
{
    close(F_ID);
    F_ID = -1; 
    return;
}

void UART_IRQHandler()
{
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  char t;
  if(tty_in(&t, 1))
  {
    extern xQueueHandle rx_queue;
    xQueueSendToBackFromISR(rx_queue, &t, &xHigherPriorityTaskWoken);
  }

  portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}
