/*
 * bsp.h
 *
 *  Created on: Nov 9, 2018
 *      Author: vchumakov
 */

#ifndef BSP_LINUX_BSP_H_
#define BSP_LINUX_BSP_H_

#include "gma.h"
#include "sd.h"
#include "ts.h"
#include "tty_uart.h"

void bsp_init(void);
uint32_t serial_get(uint8_t *c);

#endif /* BSP_LINUX_BSP_H_ */
