/*
 * ts.c
 *
 *  Created on: Nov 9, 2018
 *      Author: vchumakov
 */

#include <SDL.h>
#include "ts.h"
#include "tty_uart.h"

bsp_ts_state_t bsp_ts_get()
{
  bsp_ts_state_t ts;
  ts.st = SDL_GetMouseState(&ts.x, &ts.y) & SDL_BUTTON(1);

  SDL_Event event;
  if(SDL_PollEvent(&event))
  {
    if(event.type == SDL_QUIT)
    {
      bsp_linux_term_cfg(0);
      exit(1);
    }
  }
  return ts;
}

