/*
 * tty_uart.h
 *
 *  Created on: Dec 6, 2017
 *      Author: netcat
 */

#ifndef TTY_UART_H_
#define TTY_UART_H_

#include <inttypes.h>
#include <termios.h>

int tty_in(char *buff, int size);
int modem_serial_out(const char *buff, int len);
int modem_serial_init(void);
int tty_open(const char *tty, speed_t speed);
void tty_close(void);

void bsp_linux_term_cfg(int silent_mode);

#endif /* TTY_UART_H_ */
