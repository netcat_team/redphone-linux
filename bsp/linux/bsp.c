/*
 * bsp.c
 *
 *  Created on: Nov 9, 2018
 *      Author: vchumakov
 */

#include "bsp.h"
#include <stdio.h>

void bsp_init()
{
  bsp_linux_term_cfg(1);
  sdl_init();
}

uint32_t serial_get(uint8_t *c)
{
  *c = getchar();
  if(*c != -1) return 1;
  else return 0;
}

void _putchar(char c)
{
  putchar(c);
}
