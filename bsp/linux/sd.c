#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "fatfs.h"

void sdhc_read_block(uint8_t *buf, uint32_t cluster)
{
  char *file = "./sdcard.img";
  FILE *f = fopen(file, "r");
  uint32_t addr = FAT_CLUSTER_SIZE * cluster;

  if(f)
  {
    fseek(f, addr, SEEK_SET);
//    printf("read cluster: %8u 0x%08x \n", cluster, addr);
    int t = fread(buf, 1, FAT_CLUSTER_SIZE, f);
    t++; // warn fix
    fclose(f);
  }
  else
  {
//    printf("no file %s \n", file);
  }
}
