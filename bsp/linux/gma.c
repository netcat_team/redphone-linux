/*
 * gma.c
 *
 *  Created on: Nov 2, 2018
 *      Author: netcat
 */
#include <SDL.h>
#include "gma.h"

SDL_Surface *screen = 0;
uint32_t p_color = 0;

void sdl_init()
{
  if(SDL_Init(SDL_INIT_VIDEO) != 0) return;
  atexit(SDL_Quit);
  screen = SDL_SetVideoMode(LCD_MAXX, LCD_MAXY, 0, SDL_DOUBLEBUF);
}

void lcd_update()
{
  SDL_FreeSurface(screen);
  SDL_Flip(screen);
}

void set_pixel(SDL_Surface *dst, uint32_t x, uint32_t y, uint32_t clr)
{
  if(dst->format->BytesPerPixel != 4) return;
  if(!dst) return;

  if(x >= LCD_MAXX || y >= LCD_MAXY) return;
  
  uint32_t *pixel = (uint32_t *)dst->pixels + (y * (dst->pitch >> 2) + x);
  uint32_t alpha = (clr >> 24) & 0xff;

  if(alpha == 0xff) *pixel = clr;
  else
  {
    uint32_t dR = (*pixel >> 16) & 0xff;
    uint32_t dG = (*pixel >> 8) & 0xff;
    uint32_t dB = (*pixel >> 0) & 0xff;

    uint32_t R = ((dR + ((((clr >> 16) & 0xff) - dR) * alpha >> 8)) << 16);
    uint32_t G = ((dG + ((((clr >>  8) & 0xff) - dG) * alpha >> 8)) << 8);
    uint32_t B = ((dB + ((((clr >>  0) & 0xff) - dB) * alpha >> 8)) << 0);

    *pixel = R | G | B;
  }

  return;
}

void painter_fill_rect_dma_clut(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  for(uint32_t ix = 0; ix < w; ix++)
  {
    for(uint32_t iy = 0; iy < h; iy++) set_pixel(screen, x + ix, y + iy, p_color);
  }
}

void painter_draw_hline_clut(uint32_t x, uint32_t y, uint32_t w)
{
  for(uint32_t ix = 0; ix < w; ix++) set_pixel(screen, x + ix, y, p_color);
}

void painter_draw_vline_clut(uint32_t x, uint32_t y, uint32_t h)
{
  for(uint32_t iy = 0; iy < h; iy++) set_pixel(screen, x, y + iy, p_color);
}

void painter_point(uint32_t x, uint32_t y)
{
  extern SDL_Surface *screen;
  set_pixel(screen, x, y, p_color);
}

void painter_set_color(uint32_t clr)
{
  p_color = clr;
}

void painter_img32(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h)
{
  const uint32_t *p = (uint32_t *)data;

  for(int32_t iy = 0; iy < h; iy++)
  {
    for(int32_t ix = 0; ix < w; ix++)
    {
      painter_set_color(*p++);
      painter_point(x + ix, y + iy);
    }
  }
}

void painter_img8(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h)
{
  for(int32_t iy = 0; iy < h; iy++)
  {
    for(int32_t ix = 0; ix < w; ix++)
    {
      painter_set_color((p_color & 0x00ffffff) | (*data++ << 24));
      painter_point(x + ix, y + iy);
    }
  }
}
