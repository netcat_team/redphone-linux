/*
 * ts.h
 *
 *  Created on: Nov 9, 2018
 *      Author: vchumakov
 */

#ifndef BSP_LINUX_TS_H_
#define BSP_LINUX_TS_H_

#include <inttypes.h>

typedef struct
{
  uint32_t st;
  int32_t x;
  int32_t y;
} bsp_ts_state_t;

bsp_ts_state_t bsp_ts_get(void);

#endif /* BSP_LINUX_TS_H_ */
