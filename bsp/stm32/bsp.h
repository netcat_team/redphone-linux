/*
 * bsp.h
 *
 *  Created on: Sep 27, 2017
 *      Author: netcat
 */

#ifndef BSP_BSP_H_
#define BSP_BSP_H_

#include "pin_conf.h"
#include "fpga_regs.h"

#include "stm32f4xx.h"
#include "hal/hw_uart.h"
#include "hal/hw_fmc.h"
#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"
#include "hal/hw_i2c.h"
#include "hal/hw_spi.h"
#include "hal/hw_ltdc.h"
#include "hal/hw_sdio.h"

#include "rm68120.h"
#include "wm8903.h"

#define I2C_EEPROM 0xA0 // ok
#define I2C_OV 0x60 // camera - fix solder
#define I2C_AUDIO 0x34 // ok
#define I2C_TS 0x41 // synaptics cts

int bsp_hw_init(void);
int bsp_init(void);
uint32_t serial_get(uint8_t *c);

#endif /* BSP_BSP_H_ */
