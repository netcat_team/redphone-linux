/*
 * gma.h
 *
 *  Created on: Nov 2, 2018
 *      Author: netcat
 */

#ifndef BSP_GMA_H_
#define BSP_GMA_H_

#include <inttypes.h>
#include "rm68120.h"

#define LCD_FRAME_BUFFER ((uint32_t)0xC0000000)

void painter_fill_rect_dma_clut(uint32_t x, uint32_t y, uint32_t w, uint32_t h);
void painter_draw_hline_clut(uint32_t x, uint32_t y, uint32_t w);
void painter_draw_vline_clut(uint32_t x, uint32_t y, uint32_t h);
void painter_point(uint32_t x, uint32_t y);
void painter_set_color(uint32_t clr);
void painter_img32(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h);
void painter_img8(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h);

#endif /* BSP_GMA_H_ */
