/*
 * dma2h.h
 *
 *  Created on: Sep 15, 2017
 *      Author: netcat
 */

#ifndef HAL_HW_DMA2D_H_
#define HAL_HW_DMA2D_H_

typedef enum
{
  DMA2D_M2M = 0,
  DMA2D_M2M_PFC = 1 << 16,
  DMA2D_M2M_BLEND = 2 << 16,
  DMA2D_R2M = 3 << 16,
} dma2d_mode_t;

typedef enum
{
  DMA2D_ARGB8888 = 0,
  DMA2D_RGB888 = 1,
  DMA2D_RGB565 = 2,
  DMA2D_ARGB1555 = 3,
  DMA2D_ARGB4444 = 4
} dma2d_color_mode_t;

typedef enum
{
  CM_ARGB8888 = 0,
  CM_RGB888 = 1,
  CM_RGB565 = 2,
  CM_ARGB1555 = 3,
  CM_ARGB4444 = 4,
  CM_L8 = 5,
  CM_AL44 = 6,
  CM_AL88 = 7,
  CM_L4 = 8,
  CM_A8 = 9,
  CM_A4 = 10
} dma2d_fg_color_mode_t;

void dma2d_rst();
void dma2d_start();
void dma2d_mode(uint32_t mode);
void dma2d_set_clr(uint32_t clr);
void dma2d_set_clut_color(uint32_t c);
void dma2d_cfg(uint32_t output_offset, uint32_t number_of_line, uint32_t pixel_in_line, uint32_t output_addr);
void dma2d_wait();
void dma2d_fg_bg_cfg_clut(uint32_t vbuf_from, uint32_t vbuf_to, uint32_t bg_offset);
void dma2d_fg_bg_cfg_cp(uint32_t vbuf_from, uint32_t vbuf_to, uint32_t bg_offset);

#endif /* HAL_HW_DMA2D_H_ */
