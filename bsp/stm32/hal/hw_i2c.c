#include <stdlib.h>
#include <stdint.h>
#include "stm32f4xx.h"
#include "hal/hw_gpio.h"
#include "hal/hw_i2c.h"
#include "hal/hw_rcc.h"

void i2c_config(I2C_TypeDef* i2c, uint32_t clk)
{
  uint32_t t;

  // set freq
  uint32_t freq_range = RCC_APB1_CLK_HZ / 1000000;
  i2c->CR2 = freq_range;

  // disable
  i2c->CR1 &= ~I2C_CR1_PE;

  if(clk <= 100000)
  {
    t = RCC_APB1_CLK_HZ / (clk << 1);
    if(t < 0x04) t = 0x04;
    i2c->TRISE = freq_range + 1;
  }
  else
  {
    t = RCC_APB1_CLK_HZ / (clk * 3);
    if(!(t & I2C_CCR_CCR)) t = 1;
    t |= I2C_CCR_FS;
    i2c->TRISE = ((freq_range * 300) / 1000) + 1;
  }
  i2c->CCR = t;

  // enable, ack enable
  i2c->CR1 |= I2C_CR1_PE | 0x0400;
  // ack addr 7bit
  i2c->OAR1 = 0x4000;
}

void i2c2_init2()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
  gpio_set_af(GPIOB, 10, GPIO_AF_I2C2);
  gpio_init_ext(GPIOB, 10, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_100MHZ, GPIO_PUPD_NOPULL);
  gpio_set_af(GPIOB, 11, GPIO_AF_I2C2);
  gpio_init_ext(GPIOB, 11, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_100MHZ, GPIO_PUPD_NOPULL);

  // clk en
  RCC->APB1ENR |= RCC_APB1ENR_I2C2EN;

  i2c_config(I2C2, 400000);
}

uint32_t i2c2_read_(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, uint8_t *buf, uint32_t len)
{
  I2C_TypeDef* I2Cx = I2C2;

  // start
  I2Cx->CR1 |= I2C_CR1_START;
  // tout I2C_EVENT_MASTER_MODE_SELECT
  I2Cx->DR = addr;
  // tout I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED

  // mem addr
  if(reg_addr_size == 4)
  {
    I2Cx->DR = reg >> 24;
    // tout I2C_EVENT_MASTER_BYTE_TRANSMITTED
    I2Cx->DR = reg >> 16;
    // tout I2C_EVENT_MASTER_BYTE_TRANSMITTED
    I2Cx->DR = reg >> 8;
    // tout I2C_EVENT_MASTER_BYTE_TRANSMITTED
  }
  else if(reg_addr_size == 2)
  {
    I2Cx->DR = reg >> 8;
    // tout I2C_EVENT_MASTER_BYTE_TRANSMITTED
  }
  I2Cx->DR = reg;
  // tout I2C_EVENT_MASTER_BYTE_TRANSMITTED

  // restart
  I2Cx->CR1 |= I2C_CR1_START;
  // tout I2C_EVENT_MASTER_MODE_SELECT
  I2Cx->DR = addr | 0x01;
  // tout I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED

  // recv data
  I2Cx->CR1 |= I2C_CR1_ACK;
  while(len--)
  {
    // tout I2C_EVENT_MASTER_BYTE_RECEIVED
    *buf++ = I2Cx->DR;
  }
  I2Cx->CR1 &= ~I2C_CR1_ACK;

  // stop
  I2Cx->CR1 |= I2C_CR1_STOP;
  // tout I2C_FLAG_STOPF
  return 0;
}
