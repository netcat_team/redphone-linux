/*
 * stm_fmc.h
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#ifndef HW_FMC_H
#define HW_FMC_H

void fmc_init(void);
void fmc_sdram_init(void);
void fmc_sram_init(void);

#endif
