/*
 * hw_ltdc.c
 *
 *  Created on: Sep 18, 2017
 *      Author: netcat
 */

#include "stm32f4xx.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_rcc.h"
#include "spl/stm32f4xx_ltdc.h"
#include "rm68120.h"
#include "gma.h"
#include "hal/hw_gpio.h"
#include "pin_conf.h"

//#define LCD_HSYNC 10
//#define LCD_HBP 20
//#define LCD_HFP 10
//#define LCD_VSYNC 2
//#define LCD_VBP 2
//#define LCD_VFP 4
#define LCD_HSYNC 1
#define LCD_HBP 1
#define LCD_HFP 1
#define LCD_VSYNC 1
#define LCD_VBP 1
#define LCD_VFP 1

void P_LCD9341_InitLTCD()
{
  LTDC_InitTypeDef       LTDC_InitStruct;

  // Clock enable
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_LTDC, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2D, ENABLE);

//  P_LCD9341_AFConfig();

  LTDC_InitStruct.LTDC_HSPolarity = LTDC_HSPolarity_AL;
  LTDC_InitStruct.LTDC_VSPolarity = LTDC_VSPolarity_AL;
  LTDC_InitStruct.LTDC_DEPolarity = LTDC_DEPolarity_AL;
  LTDC_InitStruct.LTDC_PCPolarity = LTDC_PCPolarity_IPC;

  LTDC_InitStruct.LTDC_BackgroundRedValue = 0;
  LTDC_InitStruct.LTDC_BackgroundGreenValue = 0;
  LTDC_InitStruct.LTDC_BackgroundBlueValue = 0;

  //---------------------------------------
  // PLLSAI einstellen (auf 6MHz)
  //---------------------------------------
  RCC_PLLSAIConfig(192, 7, 4);
  RCC_LTDCCLKDivConfig(RCC_PLLSAIDivR_Div16);

  RCC_PLLSAICmd(ENABLE);
  while(RCC_GetFlagStatus(RCC_FLAG_PLLSAIRDY) == RESET);

  LTDC_InitStruct.LTDC_HorizontalSync = LCD_HSYNC - 1; // HSync-1
  LTDC_InitStruct.LTDC_VerticalSync = LCD_VSYNC - 1; // VSync-1
  LTDC_InitStruct.LTDC_AccumulatedHBP = LCD_HSYNC + LCD_HBP - 1; // HSync+HBP-1
  LTDC_InitStruct.LTDC_AccumulatedVBP = LCD_VSYNC + LCD_VBP - 1; // VSync+VBP-1
  LTDC_InitStruct.LTDC_AccumulatedActiveW = LCD_MAXX + (LCD_HSYNC + LCD_HBP - 1); // HSync+HBP+W-1
  LTDC_InitStruct.LTDC_AccumulatedActiveH = LCD_MAXY + (LCD_VSYNC + LCD_VBP - 1); // VSync+VBP+H-1
  LTDC_InitStruct.LTDC_TotalWidth = LCD_MAXX + (LCD_HSYNC + LCD_HBP + LCD_HFP - 1); // HSync+HBP+W+HFP-1
  LTDC_InitStruct.LTDC_TotalHeigh = LCD_MAXY + (LCD_VSYNC + LCD_VBP + LCD_VFP - 1); // VSync+VBP+H+VFP-1

  LTDC_Init(&LTDC_InitStruct);
}

void UB_LCD_LayerInit_Fullscreen()
{
  LTDC_Layer_InitTypeDef LTDC_Layer_InitStruct;

  LTDC_Layer_InitStruct.LTDC_HorizontalStart = 0;
  LTDC_Layer_InitStruct.LTDC_HorizontalStop = LCD_MAXX - 1;
  LTDC_Layer_InitStruct.LTDC_VerticalStart = 0;
  LTDC_Layer_InitStruct.LTDC_VerticalStop = LCD_MAXY - 1;

  LTDC_Layer_InitStruct.LTDC_PixelFormat = LTDC_Pixelformat_RGB565;
  LTDC_Layer_InitStruct.LTDC_ConstantAlpha = 255;
  LTDC_Layer_InitStruct.LTDC_DefaultColorBlue = 0;
  LTDC_Layer_InitStruct.LTDC_DefaultColorGreen = 0;
  LTDC_Layer_InitStruct.LTDC_DefaultColorRed = 0;
  LTDC_Layer_InitStruct.LTDC_DefaultColorAlpha = 0;

  LTDC_Layer_InitStruct.LTDC_BlendingFactor_1 = LTDC_BlendingFactor1_CA;
  LTDC_Layer_InitStruct.LTDC_BlendingFactor_2 = LTDC_BlendingFactor2_CA;

  LTDC_Layer_InitStruct.LTDC_CFBLineLength = LCD_MAXX * LCD_COLOR_SIZE;
  LTDC_Layer_InitStruct.LTDC_CFBPitch = LCD_MAXX * LCD_COLOR_SIZE;
  LTDC_Layer_InitStruct.LTDC_CFBLineNumber = LCD_MAXY;

  LTDC_Layer_InitStruct.LTDC_CFBStartAdress = LCD_FRAME_BUFFER;
  LTDC_LayerInit(LTDC_Layer1, &LTDC_Layer_InitStruct);

  LTDC_Layer_InitStruct.LTDC_CFBStartAdress = LCD_FRAME_BUFFER + LCD_FRAME_OFFSET;

  LTDC_Layer_InitStruct.LTDC_BlendingFactor_1 = LTDC_BlendingFactor1_PAxCA;
  LTDC_Layer_InitStruct.LTDC_BlendingFactor_2 = LTDC_BlendingFactor2_PAxCA;

  LTDC_LayerInit(LTDC_Layer2, &LTDC_Layer_InitStruct);

  LTDC_ReloadConfig(LTDC_IMReload);

  LTDC_LayerCmd(LTDC_Layer1, ENABLE);
//  LTDC_LayerCmd(LTDC_Layer2, ENABLE);

  LTDC_ReloadConfig(LTDC_IMReload);

  LTDC_DitherCmd(ENABLE);

  LTDC_Cmd(ENABLE);
}

void ltdc_init()
{
  P_LCD9341_InitLTCD();
  UB_LCD_LayerInit_Fullscreen();

  LTDC_ITConfig(LTDC_IT_LI, ENABLE);
  NVIC_EnableIRQ(LTDC_IRQn);
}

void LTDC_IRQHandler()
{
  LTDC_ClearITPendingBit(LTDC_IT_LI);
//  LTDC_ITConfig(LTDC_IT_FU, DISABLE);

//  gpio_toggle(SYS_LED_GPIO, SYS_LED);
}

