#include <stdlib.h>
#include <stdint.h>
#include "stm32f4xx.h"
#include "hal/hw_gpio.h"
#include "hal/hw_spi.h"
#include "hal/hw_rcc.h"

// spi1
gpio_init_t gpio_spi1_init[] = {
  { GPIOA, 7 }, // MOSI
  { GPIOA, 6 }, // MISO
  { GPIOA, 5 }, // SCK
  {}};

// spi2
gpio_init_t gpio_spi2_init[] = {
  { GPIOB, 15 }, // MOSI
  { GPIOB, 14 }, // MISO
  { GPIOB, 13 }, // SCK
  {}};

// spi3
gpio_init_t gpio_spi3_init[] = {
  { GPIOC, 10 }, // SCK
  { GPIOC, 11 }, // MISO
  { GPIOC, 12 }, // MOSI
  {}};

void spi_config(SPI_TypeDef *spi)
{
  // 0 for slave
  spi->CR1 = SPI_CR1_MSTR | // master mode
             SPI_CR1_SSM | // soft NSS
             SPI_CR1_BR_2; // div 32

  // IE
//  spi->CR2 = SPI_CR2_RXNEIE | SPI_CR2_TXEIE;
  // clear flags
  spi->SR &= ~(SPI_SR_RXNE | SPI_SR_TXE);

  // spi mode (reset I2SMOD)
  spi->I2SCFGR &= ~SPI_I2SCFGR_I2SMOD;
  // poly size
  spi->CRCPR = 7;
  // en
  spi->CR1 |= SPI_CR1_SPE;
}

void spi1_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
  gpio_init_af(gpio_spi1_init, GPIO_AF_SPI1);

  // clk en
  RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
  spi_config(SPI1);
}

void spi1_deinit()
{
  SPI1->CR1 = 0;
  RCC->APB2ENR &= ~RCC_APB2ENR_SPI1EN;
  gpio_deinit_af(gpio_spi1_init);
}

void spi2_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
  gpio_init_af(gpio_spi2_init, GPIO_AF_SPI2);

  // clk en
  RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
  spi_config(SPI2);
}

void spi3_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
  gpio_init_af(gpio_spi3_init, GPIO_AF_SPI3);

  // clk en
  RCC->APB1ENR |= RCC_APB1ENR_SPI3EN;
  spi_config(SPI3);
}

static inline uint8_t spi_io(SPI_TypeDef *SPIx, uint8_t data)
{
  while(!(SPIx->SR & SPI_I2S_FLAG_TXE));
  SPIx->DR = data;
  while(!(SPIx->SR & SPI_I2S_FLAG_RXNE));
  return SPIx->DR;
}

uint8_t spi1_io(uint8_t data)
{
  return spi_io(SPI1, data);
}

uint8_t spi2_io(uint8_t data)
{
  return spi_io(SPI2, data);
}

uint8_t spi3_io(uint8_t data)
{
  return spi_io(SPI3, data);
}
