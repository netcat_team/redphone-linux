/*
 * hw_uart.c
 *
 *  Created on: Feb 8, 2017
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>
#include "stm32f429xx.h"
#include "hal/hw_uart.h"
#include "hal/hw_gpio.h"
#include "hal/hw_rcc.h"

gpio_init_t gpio_uart1_init[] = {
  // USART1
  { GPIOB, 6 }, // UART_EXT_TX
  { GPIOB, 7 }, // UART_EXT_RX
  {}};

gpio_init_t gpio_uart6_init[] = {
  // USART6
  { GPIOC, 7 }, // UART_EXT_TX
  { GPIOC, 6 }, // UART_EXT_RX
  {}};

static void usart_config(USART_TypeDef *uart, uint32_t baudrate)
{
  // cfg
  uart->CR1 = USART_CR1_TE | USART_CR1_RE;
  uart->CR2 = 0;
  uart->CR3 = 0;

  // rx ie
  uart->CR1 |= USART_CR1_RXNEIE;

  // baud
  uint32_t int_div, frac_div, t;
  int_div = (25 * RCC_APB1_CLK_HZ) / (baudrate << 1);
  t = (int_div / 100) << 4;
  frac_div = int_div - 100 * (t >> 4);
  t |= ((frac_div * 16 + 50) / 100) & 0x0f;
  uart->BRR = t;

  // en
  uart->CR1 |= USART_CR1_UE;
}

void usart1_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
  gpio_init_af(gpio_uart1_init, GPIO_AF_USART1);

  // clk en
  RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

  usart_config(USART1, UART1_BAUDRATE);
}

void usart6_init()
{
  // gpio cfg
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
  gpio_init_af(gpio_uart6_init, GPIO_AF_USART6);

  // clk en
  RCC->APB2ENR |= RCC_APB2ENR_USART6EN;

  usart_config(USART6, UART6_BAUDRATE);
}

void usart_out(USART_TypeDef *uart, uint8_t data)
{
  while(!(uart->SR & USART_SR_TXE));
  uart->DR = data;
}
