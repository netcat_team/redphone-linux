/*
 * hw_spi.h
 *
 *  Created on: Jul 19, 2017
 *      Author: netcat
 */

#ifndef HAL_HW_SPI_H_
#define HAL_HW_SPI_H_

#define SPI_I2S_FLAG_RXNE 0x0001
#define SPI_I2S_FLAG_TXE 0x0002

void spi1_init(void);
void spi1_deinit(void);
void spi2_init(void);
void spi3_init(void);
uint8_t spi1_io(uint8_t data);
uint8_t spi2_io(uint8_t data);
uint8_t spi3_io(uint8_t data);

#endif /* HAL_HW_SPI_H_ */
