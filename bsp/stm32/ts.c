/*
 * ts.c
 *
 *  Created on: Nov 13, 2018
 *      Author: vchumakov
 */


#include "stm32f4xx.h"

#include "ub_lib/stm32_ub_i2c3.h"
#include "hal/hw_i2c.h"
#include "hal/hw_gpio.h"
#include "bsp.h"

#include "ts.h"

bsp_ts_state_t bsp_ts_get()
{
  bsp_ts_state_t ts;
  uint8_t data[5];

  i2c2_read(0x41, 0x15, 1, data, 5);

  ts.st = data[0] & 0x01;
  ts.x = data[3] * 7;
  ts.y = data[4] * 7;

  return ts;
}

