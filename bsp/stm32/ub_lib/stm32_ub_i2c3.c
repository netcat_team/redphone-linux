//--------------------------------------------------------------
// File     : stm32_ub_i2c3.c
// Datum    : 17.12.2013
// Version  : 1.1
// Autor    : UB
// EMail    : mc-4u(@)t-online.de
// Web      : www.mikrocontroller-4u.de
// CPU      : STM32F429
// IDE      : CooCox CoIDE 1.7.4
// GCC      : 4.7 2012q4
// Module   : GPIO, I2C 
// Funktion : I2C-LoLevel-Funktionen (I2C-3)
//
// Hinweis  : m�gliche Pinbelegungen
//            I2C2 : SCL :[PA8]
//                   SDA :[PC9]
//            externe PullUp-Widerst�nde an SCL+SDA notwendig
//--------------------------------------------------------------


//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
#include "stm32_ub_i2c3.h"
//#include "stm32f4xx_gpio.h"

void i2c2_init()
{
//  GPIO_InitTypeDef GPIO_InitStructure;
//
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
//  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
//  GPIO_Init(GPIOB, &GPIO_InitStructure);
//
//  GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_I2C2);
//  GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_I2C2);

  I2C_DeInit(I2C2);

  I2C_InitTypeDef I2C_InitStructure;

  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
  I2C_InitStructure.I2C_OwnAddress1 = 0;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  I2C_InitStructure.I2C_ClockSpeed = 400000;

  I2C_Cmd(I2C2, ENABLE);
  I2C_Init(I2C2, &I2C_InitStructure);
}

#define i2c_timeout(x) timeout_cnt = 0xFFFF; while (x) { if (timeout_cnt-- == 0) goto errReturn; }

uint8_t i2c2_read(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, uint8_t *buf, uint32_t len)
{
  uint32_t timeout_cnt = 0;
  if(!len) return 0;
  i2c_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_BUSY));

  // start
  I2C_GenerateSTART(I2C2, ENABLE);
  i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT));

  // addr
  I2C_Send7bitAddress(I2C2, addr, I2C_Direction_Transmitter);
  i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

  // mem addr
  if(reg_addr_size == 4)
  {
    I2C_SendData(I2C2, reg >> 24);
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg >> 16);
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg >> 8);
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg);
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else if(reg_addr_size == 2)
  {
    I2C_SendData(I2C2, reg >> 8);
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg);
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else
  {
    I2C_SendData(I2C2, reg);
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }

  // restart
  I2C_GenerateSTART(I2C2, ENABLE);
  i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT));

  // Send Address
  I2C_Send7bitAddress(I2C2, addr, I2C_Direction_Receiver);
  i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

  // recv data
  I2C_AcknowledgeConfig(I2C2, ENABLE);
  while(len--)
  {
    i2c_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED));
    *buf++ = I2C_ReceiveData(I2C2);
  }
  I2C_AcknowledgeConfig(I2C2, DISABLE);

  // Wait for stop
  I2C_GenerateSTOP(I2C2, ENABLE);
  i2c_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_STOPF));
  return 0;

  errReturn:
  I2C_GenerateSTOP(I2C2, ENABLE);
  i2c_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_STOPF));
  return 1;
}


