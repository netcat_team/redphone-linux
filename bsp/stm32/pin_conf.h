/*
 * pin_config.h
 *
 *  Created on: Jul 23, 2017
 *      Author: netcat
 */

#ifndef BSP_PIN_CONF_H_
#define BSP_PIN_CONF_H_

#define FPGA_EN_GPIO GPIOB
#define FPGA_EN_PIN 12

#define PWR_SHDN_GPIO GPIOG
#define PWR_SHDN_PIN 10

#define GSM_GPIO GPIOG
#define GSM_EN_PIN 6
#define GSM_PWR_KEY 7

#define PWR_OTG_MODE_GPIO GPIOG
#define PWR_OTG_MODE_PIN 11

#define NRF_GPIO GPIOD
#define NRF_CS 13
#define NRF_IRQ 12
#define NRF_CE 11

#define PWR_BUT_PORT GPIOD
#define PWR_BUT_PIN 6

#define SYS_LED_GPIO GPIOB
#define SYS_LED 9

#endif /* BSP_PIN_CONF_H_ */
