/*
 * fpga_regs.h
 *
 *  Created on: Sep 27, 2017
 *      Author: netcat
 */

#ifndef BSP_FPGA_REGS_H_
#define BSP_FPGA_REGS_H_

typedef struct
{
  volatile uint16_t LCD;
  volatile uint16_t BL;
} FPGA_SYS_CFG_t;

#define FPGA_ADDR_BASE ((uint32_t)0x64000000)
#define FPGA_SYS_CFG ((FPGA_SYS_CFG_t *)FPGA_ADDR_BASE)

#endif /* BSP_FPGA_REGS_H_ */
