/*
 * painter.h
 *
 *  Created on: Jan 4, 2016
 *      Author: netcat
 */

#ifndef RM68120_H_
#define RM68120_H_

#define LCD_ADDR_REG ((uint32_t)0x60000000)
#define LCD_ADDR_DATA ((uint32_t)0x60004000)
#define LCD_WRITE_REG(addr) (*(volatile uint16_t*)LCD_ADDR_REG) = addr
#define LCD_WRITE_DATA(data) (*(volatile uint16_t*)LCD_ADDR_DATA) = data
#define LCD_READ_DATA() (*(uint16_t*)(0x60004000))
#define LCD_ADDR_REG ((uint32_t)0x60000000)

#define LCD_MAXX ((uint32_t)480)
#define LCD_MAXY ((uint32_t)800)
#define LCD_SIZE ((uint32_t)(LCD_MAXX * LCD_MAXY))
#define LCD_COLOR_SIZE ((uint32_t)2)
#define LCD_FRAME_OFFSET ((uint32_t)(LCD_SIZE * LCD_COLOR_SIZE))

void lcd_dma_init();
void lcd_init();
void lcd_cfg_fsmc();
void lcd_cfg(uint32_t LCD_Reg, uint8_t LCD_Value);
void lcd_set_addr0();
void lcd_dma_ini_cp(uint32_t addr, uint32_t size);
void lcd_update();

#endif /* RM68120_H_ */
