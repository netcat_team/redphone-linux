/*
 * bsp.c
 *
 *  Created on: Sep 28, 2017
 *      Author: netcat
 */

#include <stdlib.h>
#include <stdint.h>

#include "bsp.h"

void modem_serial_out(const char *buff, int len)
{
  int i;
  for(i = 0; i < len; i++) usart_out(USART6, buff[i]);
}

void _putchar(char c)
{
  usart_out(USART6, c);
}

uint32_t serial_get(uint8_t *c)
{
  uint32_t ret = USART1->SR & USART_SR_RXNE;
  if(ret)
  {
    USART1->SR &= ~USART_SR_RXNE;
    *c = USART1->DR;
  }
  return ret;
}

int modem_serial_init()
{
  return 0;
}

int bsp_hw_init()
{
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIODEN;
  
  gpio_init(SYS_LED_GPIO, SYS_LED, GPIO_MODE_OUT);
  gpio_init(FPGA_EN_GPIO, FPGA_EN_PIN, GPIO_MODE_OUT);
  gpio_init(PWR_SHDN_GPIO, PWR_SHDN_PIN, GPIO_MODE_OUT);
  gpio_init(GSM_GPIO, GSM_EN_PIN, GPIO_MODE_OUT);
  gpio_init(GSM_GPIO, GSM_PWR_KEY, GPIO_MODE_OUT);
  gpio_init(PWR_OTG_MODE_GPIO, PWR_OTG_MODE_PIN, GPIO_MODE_OUT);

  gpio_reset(SYS_LED_GPIO, SYS_LED);
  gpio_set(PWR_SHDN_GPIO, PWR_SHDN_PIN);
  gpio_reset(FPGA_EN_GPIO, FPGA_EN_PIN);
  gpio_reset(GSM_GPIO, GSM_EN_PIN);
  gpio_reset(GSM_GPIO, GSM_PWR_KEY);
  gpio_reset(PWR_OTG_MODE_GPIO, PWR_OTG_MODE_PIN);

  i2c2_init2();
  spi2_init();

  fmc_init();
  fmc_sdram_init();
  fmc_sram_init();

  lcd_init();

  RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN | RCC_AHB1ENR_DMA2DEN;

  gpio_init_ext(PWR_BUT_PORT, PWR_BUT_PIN, GPIO_MODE_IN, GPIO_OTYPE_PP, GPIO_SPEED_100MHZ, GPIO_PUPD_UP);

  return 0;
}

int bsp_init()
{
  set_sys_clock();
  bsp_hw_init();
  // painter_clear();
  lcd_update();
  // console_init(); // make CLI task

  NVIC_SetPriority(SDIO_IRQn, 14);
  NVIC_EnableIRQ(SDIO_IRQn);
  NVIC_SetPriority(USART1_IRQn, 14);
  NVIC_EnableIRQ(USART1_IRQn);
  NVIC_SetPriority(USART6_IRQn, 14);
  NVIC_EnableIRQ(USART6_IRQn);

  gpio_reset(SYS_LED_GPIO, SYS_LED);

  return 0;
}

