/*
 * gma.c
 *
 *  Created on: Nov 13, 2018
 *      Author: netcat
 */
#include "gma.h"
#include "stm32f4xx.h"
#include "rm68120.h"
#include "hal/hw_dma2d.h"

uint32_t vbuf = LCD_FRAME_BUFFER;
uint32_t vbuf_clut = LCD_FRAME_BUFFER + LCD_FRAME_OFFSET;

extern int32_t vbuf_minx;
extern int32_t vbuf_miny;
extern int32_t vbuf_maxx;
extern int32_t vbuf_maxy;

void painter_fast_fill_rect_dma(uint32_t vbuf_to, uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t c)
{
  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_R2M);
  dma2d_set_clr(c);
  dma2d_cfg(LCD_MAXX - w, h, w, vbuf_to + offset);
  dma2d_start();
  dma2d_wait();
}

void painter_copy_layer_rect(uint32_t vbuf_to, uint32_t vbuf_from, uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  dma2d_rst();
  dma2d_mode(DMA2D_M2M);
  dma2d_cfg(LCD_MAXX - w, h, w, vbuf_to);
  dma2d_fg_bg_cfg_cp(vbuf_from, vbuf_to, LCD_MAXX - w);

  dma2d_start();
  dma2d_wait();
}

void painter_fill_rect_dma_clut(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(LCD_MAXX - w, h, w, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, LCD_MAXX - w);

  dma2d_start();
  dma2d_wait();
}

void painter_draw_hline_clut(uint32_t x, uint32_t y, uint32_t w)
{
  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(0, 1, w, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, 0);

  dma2d_start();
  dma2d_wait();
}

void painter_draw_vline_clut(uint32_t x, uint32_t y, uint32_t h)
{
  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(LCD_MAXX - 1, h, 1, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, LCD_MAXX - 1);

  dma2d_start();
  dma2d_wait();
}

void painter_point(uint32_t x, uint32_t y)
{
  if(x < vbuf_minx || x >= vbuf_maxx || y < vbuf_miny || y >= vbuf_maxy) return;

  uint32_t offset = LCD_COLOR_SIZE * (LCD_MAXX * y + x);

  dma2d_rst();
  dma2d_mode(DMA2D_M2M_BLEND);
  dma2d_cfg(0, 1, 1, vbuf + offset);
  dma2d_fg_bg_cfg_clut(vbuf_clut, vbuf + offset, 0);

  dma2d_start();
  dma2d_wait();
}

void painter_set_color(uint32_t clr)
{
  dma2d_set_clut_color(clr);
}

void painter_img32(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h)
{

}

void painter_img8(const uint8_t *data, int32_t x, int32_t y, int32_t w, int32_t h)
{

}
