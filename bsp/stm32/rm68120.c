
#include "stm32f4xx.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_rcc.h"
#include "stm32_ub_i2c3.h"
#include "gma.h"
#include "rm68120.h"
#include "hal/hw_gpio.h"
#include "fpga_regs.h"

void delay_cyc(volatile unsigned int cnt) { while(cnt--); }


#define SDRAM_READ_DATA(addr) (*(volatile uint16_t*)(LCD_FRAME_BUFFER + addr))
#define LCD_FRAME_WRITE_DATA(addr, data) (*(volatile uint16_t*)(LCD_FRAME_BUFFER + addr)) = data;

void lcd_dma_ini_cp2(uint32_t addr, uint32_t size)
{
  uint32_t reg;
  DMA_DeInit(DMA2_Stream0);
  while(DMA_GetCmdStatus(DMA2_Stream0) != DISABLE);

  DMA_InitTypeDef DMA_InitStructure;
  DMA_InitStructure.DMA_Channel = DMA_Channel_0;
  DMA_InitStructure.DMA_PeripheralBaseAddr = addr;
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&reg;
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToMemory;
  DMA_InitStructure.DMA_BufferSize = size; // size bytes 0x5DC00
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA2_Stream0, &DMA_InitStructure);

  DMA_Cmd(DMA2_Stream0, ENABLE);
  while(DMA_GetCmdStatus(DMA2_Stream0) != DISABLE);
}

void fpga_lcd_copy()
{
  // LCD_AQ13
  gpio_set(GPIOG, 3);
  gpio_init(GPIOG, 3, GPIO_MODE_OUT);
  // LCD_NWE (WR)
  gpio_init(GPIOD, 5, GPIO_MODE_IN);
  // LCD_NE1 (CS)
  gpio_set(GPIOG, 7);
  gpio_init(GPIOD, 7, GPIO_MODE_OUT);

  FPGA_SYS_CFG->LCD = 1;

  delay_cyc(100);

  lcd_dma_ini_cp2(LCD_FRAME_BUFFER + 0xfa00 * 0, 0xfa00);
  lcd_dma_ini_cp2(LCD_FRAME_BUFFER + 0xfa00 * 2, 0xfa00);
  lcd_dma_ini_cp2(LCD_FRAME_BUFFER + 0xfa00 * 4, 0xfa00);
  lcd_dma_ini_cp2(LCD_FRAME_BUFFER + 0xfa00 * 6, 0xfa00);
  lcd_dma_ini_cp2(LCD_FRAME_BUFFER + 0xfa00 * 8, 0xfa00);
  lcd_dma_ini_cp2(LCD_FRAME_BUFFER + 0xfa00 * 10, 0xfa00);

  delay_cyc(100);

  FPGA_SYS_CFG->LCD = 0;

  delay_cyc(100);

  gpio_init(GPIOG, 3, GPIO_MODE_AF);
  gpio_init(GPIOD, 5, GPIO_MODE_AF);
  gpio_init(GPIOD, 7, GPIO_MODE_AF);

  // FPGA_NE2
//  gpio_set(GPIOG, 9);
//  gpio_init_ext(GPIOG, 9, GPIO_MODE_OUT, GPIO_OTYPE_PP, GPIO_SPEED_100MHZ, GPIO_PUPD_NOPULL);
//  ltdc_init();
}

void lcd_dma_ini_cp(uint32_t addr, uint32_t size)
{
  DMA_DeInit(DMA2_Stream0);
  while(DMA_GetCmdStatus(DMA2_Stream0) != DISABLE);

  DMA_InitTypeDef DMA_InitStructure;
  DMA_InitStructure.DMA_Channel = DMA_Channel_0;
  DMA_InitStructure.DMA_PeripheralBaseAddr = addr; // from
  DMA_InitStructure.DMA_Memory0BaseAddr = LCD_ADDR_DATA;  // to
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToMemory;
  DMA_InitStructure.DMA_BufferSize = size; // size bytes 0x5DC00
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA2_Stream0, &DMA_InitStructure);

//  DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);
//  NVIC_EnableIRQ(DMA2_Stream0_IRQn);

  DMA_Cmd(DMA2_Stream0, ENABLE);
  while(DMA_GetCmdStatus(DMA2_Stream0) != DISABLE);
}

void lcd_update()
{
#if 1
  lcd_dma_ini_cp(LCD_FRAME_BUFFER + 0xfa00 * 0, 0xfa00);
  lcd_dma_ini_cp(LCD_FRAME_BUFFER + 0xfa00 * 2, 0xfa00);
  lcd_dma_ini_cp(LCD_FRAME_BUFFER + 0xfa00 * 4, 0xfa00);
  lcd_dma_ini_cp(LCD_FRAME_BUFFER + 0xfa00 * 6, 0xfa00);
  lcd_dma_ini_cp(LCD_FRAME_BUFFER + 0xfa00 * 8, 0xfa00);
  lcd_dma_ini_cp(LCD_FRAME_BUFFER + 0xfa00 * 10, 0xfa00);
#else
// why bad ?
//  painter_copy_layer_rect(SDRAM_START_ADR, SDRAM_START_ADR, 0, 0, LCD_MAXX, LCD_MAXY);

//  painter_copy_layer_rect(LCD_FRAME_BUFFER + LCD_FRAME_OFFSET, LCD_FRAME_BUFFER, 0, 0, LCD_MAXX, LCD_MAXY);
  fpga_lcd_copy();
#endif
}

void lcd_cfg_fsmc(void)
{
  RCC->AHB1LPENR |= RCC_AHB1LPENR_SRAM1LPEN;

  FMC_Bank1->BTCR[0] =
    0x00000002 | // data addes mux enabled
    0x00000010 | // memory data width 16b
    0x00000100 | // burst access mode enabled
    0x00001000 | // write operation enabled
    0x00100000;  // write operation enabled

  // address setup time 1
  FMC_Bank1->BTCR[1] = 1;
  FMC_Bank1E->BWTR[0] = 1; // 0x0FFFFFFF

  // enable
  FMC_Bank1->BTCR[0] |= 0x01;

//  FMC_NORSRAMInitTypeDef fmc_nor_init;
//  FMC_NORSRAMTimingInitTypeDef fmc_nor_t_init;
//
//  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_SRAM1, ENABLE);
//
//  // FSMC_Bank1_NORSRAM2 configuration
//  fmc_nor_t_init.FMC_AddressSetupTime = 1;
//  fmc_nor_t_init.FMC_AddressHoldTime = 0;
//  fmc_nor_t_init.FMC_DataSetupTime = 0;
//  fmc_nor_t_init.FMC_BusTurnAroundDuration = 0;
//  fmc_nor_t_init.FMC_CLKDivision = 0;
//  fmc_nor_t_init.FMC_DataLatency = 0;
//  fmc_nor_t_init.FMC_AccessMode = FMC_AccessMode_A;
//
//  fmc_nor_init.FMC_Bank = FMC_Bank1_NORSRAM1;
//  fmc_nor_init.FMC_DataAddressMux = FMC_DataAddressMux_Enable;
//  fmc_nor_init.FMC_MemoryType = FMC_MemoryType_SRAM;
//  fmc_nor_init.FMC_MemoryDataWidth = FMC_NORSRAM_MemoryDataWidth_16b;
//  fmc_nor_init.FMC_BurstAccessMode = FMC_BurstAccessMode_Enable;
//  fmc_nor_init.FMC_AsynchronousWait = FMC_AsynchronousWait_Disable;
//  fmc_nor_init.FMC_WaitSignalPolarity = FMC_WaitSignalPolarity_Low;
//  fmc_nor_init.FMC_WrapMode = FMC_WrapMode_Enable;
//  fmc_nor_init.FMC_WaitSignalActive = FMC_WaitSignalActive_BeforeWaitState;
//  fmc_nor_init.FMC_WriteOperation = FMC_WriteOperation_Enable;
//  fmc_nor_init.FMC_WaitSignal = FMC_WaitSignal_Disable;
//  fmc_nor_init.FMC_ExtendedMode = FMC_ExtendedMode_Enable;
//  fmc_nor_init.FMC_WriteBurst = FMC_WriteBurst_Enable;
//  fmc_nor_init.FMC_ContinousClock = FMC_CClock_SyncAsync;
//  fmc_nor_init.FMC_ReadWriteTimingStruct = &fmc_nor_t_init;
//  fmc_nor_init.FMC_WriteTimingStruct = &fmc_nor_t_init;
//
//  FMC_NORSRAMInit(&fmc_nor_init);
//
//  // Enable FSMC NOR/SRAM Bank2
//  FMC_NORSRAMCmd(FMC_Bank1_NORSRAM1, ENABLE);
}

void lcd_cfg(uint32_t LCD_Reg, uint8_t LCD_Value)
{
//  uint16_t reg = ((LCD_Reg << 8) & 0xff00) | ((LCD_Reg >> 8) & 0xff);
  LCD_WRITE_REG(LCD_Reg);
//  uint16_t val = ((LCD_Value << 8) & 0xff00) | ((LCD_Value >> 8) & 0xff);
  LCD_WRITE_DATA(LCD_Value);
}

void lcd_set_addr0()
{
  lcd_cfg(0x2a00, 0);
  lcd_cfg(0x2a01, 0);
  lcd_cfg(0x2a02, 0);
  lcd_cfg(0x2a03, 0);

  lcd_cfg(0x2b00, 0);
  lcd_cfg(0x2b01, 0);
  lcd_cfg(0x2b02, 0);
  lcd_cfg(0x2b03, 0);

  lcd_cfg(0x2c00, 0);
}

void lcd_init()
{
  lcd_cfg(0xF000, 0x55);//Enable Page 1
  lcd_cfg(0xF001, 0xAA);
  lcd_cfg(0xF002, 0x52);
  lcd_cfg(0xF003, 0x08);
  lcd_cfg(0xF004, 0x01);

  lcd_cfg(0xB600, 0x34);//AVDD Setting
  lcd_cfg(0xB601, 0x34);
  lcd_cfg(0xB602, 0x34);
  lcd_cfg(0xB000, 0x0c);
  lcd_cfg(0xB001, 0x0c);
  lcd_cfg(0xB002, 0x0c);

  lcd_cfg(0xB700, 0x34);//AVEE Setting
  lcd_cfg(0xB701, 0x34);
  lcd_cfg(0xB702, 0x34);
  lcd_cfg(0xB100, 0x0d);
  lcd_cfg(0xB101, 0x0d);
  lcd_cfg(0xB102, 0x0d);

  lcd_cfg(0xB800, 0x24);//VCL Setting
  lcd_cfg(0xB200, 0x00);

  lcd_cfg(0xBf00, 0x01);

  lcd_cfg(0xB900, 0x24);//VGH Setting
  lcd_cfg(0xB901, 0x24);
  lcd_cfg(0xB902, 0x24);
  lcd_cfg(0xB300, 0x05);
  lcd_cfg(0xB301, 0x05);
  lcd_cfg(0xB302, 0x05);

  lcd_cfg(0xBA00, 0x34);//VGL Setting
  lcd_cfg(0xBA01, 0x34);
  lcd_cfg(0xBA02, 0x34);
  lcd_cfg(0xB500, 0x0b);
  lcd_cfg(0xB501, 0x0b);
  lcd_cfg(0xB502, 0x0b);


  lcd_cfg(0xBC00, 0x00);//VGMP Setting
  lcd_cfg(0xBC01, 0x80);
  lcd_cfg(0xBC02, 0x00);

  lcd_cfg(0xBD00, 0x00);//VGMN Setting
  lcd_cfg(0xBD01, 0x80);
  lcd_cfg(0xBD02, 0x00);

  lcd_cfg(0xBE00, 0x00);//Vcom Setting
  lcd_cfg(0xBE01, 0x59);//2

  lcd_cfg(0xB400, 0x10);

  lcd_cfg(0xB000, 0x08); //RGB I/F Setting
  lcd_cfg(0xB001, 0x05);
  lcd_cfg(0xB002, 0x02);
  lcd_cfg(0xB003, 0x05);
  lcd_cfg(0xB004, 0x02);

  lcd_cfg(0xB600, 0x05);//SDT


  lcd_cfg(0xFF00, 0xAA);
  lcd_cfg(0xFF01, 0x55);
  lcd_cfg(0xFF02, 0x25);
  lcd_cfg(0xFF03, 0x01);

  //lcd_cfg(0xF304, 0x11);
  //lcd_cfg(0xF306, 0x10);
  //lcd_cfg(0xF408, 0x00);

//  lcd_cfg(0x3500, 0x00);
  lcd_cfg(0x3600, 0xD4); // rotate
  lcd_cfg(0x3A00, 0x55);
  //lcd_cfg(0x3B00, 0x28);

  lcd_cfg(0x1100, 0x00);  //Sleep out
  delay_cyc(1000);
  lcd_cfg(0x2900, 0x00); //Display on
  delay_cyc(1000);
  lcd_cfg(0x2c00, 0x00);
}
